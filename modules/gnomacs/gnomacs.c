/*
 * Very simple sample module. Illustrates most of the salient features
 * of Emacs dynamic modules.
 * (C) Copyright 1998, 1999 J. Kean Johnston. All rights reserved.
 */

#include <gnomacs.h>
#include <GnomacsFrame.h>
#include <dlfcn.h>

static GnomacsFunctions *gnomacs_functions = NULL;

Lisp_Object Qgnomacs_minor_mode;
Lisp_Object Qgnomacs_init_hook;
Lisp_Object Qgnomacs_visit_hook;
Lisp_Object Qgnomacs_done_hook;

Lisp_Object Qframe_created_hook;
Lisp_Object Qgnomacs_make_frame;

Lisp_Object Qnormal;

DEFUN ("gnomacs-init", Fgnomacs_init, 0, 0, "", /* */ ())
{
    static int initialized = 0;
    void *handle, (*func_ptr) (void), *ptr;

    if (initialized) return;

    fprintf (stderr, "\nInitializing Gnomacs!\n");

    handle = dlopen ("libgnomacs.so", RTLD_LAZY|RTLD_GLOBAL);

    fprintf (stderr, "HANDLE: %p\n", handle);

    if (!handle) {
      fprintf (stderr, "ERROR: |%s|\n", dlerror ());
      return;
    }

    func_ptr = dlsym (handle, "gnomacs_gnome_init");
    if (!func_ptr) return;

    fprintf (stderr, "FUNC_PTR: %p\n", func_ptr);

    stop_interrupts ();
    func_ptr ();
    start_interrupts ();

    ptr = dlsym (handle, "GnomacsFunctionTable");
    if (!handle) {
      fprintf (stderr, "ERROR: |%s|\n", dlerror ());
      return;
    }

    gnomacs_functions = ptr;

#if 0
    console_type_create_toolbar_bonobo ();
#endif

    initialized = 1;
}

DEFUN ("gnomacs-idle-loop", Fgnomacs_idle_loop, 0, 0, "", /* */ ())
{
    if (!gnomacs_functions || !gnomacs_functions->idle_loop)
	return;

    gnomacs_functions->idle_loop ();
}

DEFUN ("gnomacs-kill-emacs", Fgnomacs_kill_emacs, 0, 0, "", /* */ ())
{
    fprintf (stderr, "Done calling kill emacs\n");
}

void
modules_of_gnomacs()
{
}

void
syms_of_gnomacs()
{
    DEFSUBR(Fgnomacs_idle_loop);
    DEFSUBR(Fgnomacs_init);
    DEFSUBR(Fgnomacs_kill_emacs);

    defsymbol (&Qgnomacs_minor_mode, "gnomacs-minor-mode");
    defsymbol (&Qgnomacs_init_hook, "gnomacs-init-hook");
    defsymbol (&Qgnomacs_visit_hook, "gnomacs-visit-hook");
    defsymbol (&Qgnomacs_done_hook, "gnomacs-done-hook");

    defsymbol (&Qframe_created_hook, "frame-created-hook");

    defsymbol (&Qgnomacs_make_frame, "gnomacs-make-frame");

    defsymbol (&Qnormal, "normal");
}

void
vars_of_gnomacs()
{
}

