/* toolbar implementation -- X interface.
   Copyright (C) 1995 Board of Trustees, University of Illinois.
   Copyright (C) 1995 Sun Microsystems, Inc.
   Copyright (C) 1995, 1996 Ben Wing.
   Copyright (C) 1996 Chuck Thompson.

This file is part of XEmacs.

XEmacs is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

XEmacs is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with XEmacs; see the file COPYING.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.  */

/* Synched up with: Not in FSF. */

#include <gnomacs.h>
#include <console-x.h>
#include <EmacsFrame.h>
#include <opaque.h>

#include <GnomacsFrame.h>

#define SET_TOOLBAR_WAS_VISIBLE_FLAG(frame, pos, flag)			\
  do {									\
    switch (pos)							\
      {									\
      case TOP_TOOLBAR:							\
	(frame)->top_toolbar_was_visible = flag;			\
	break;								\
      case BOTTOM_TOOLBAR:						\
	(frame)->bottom_toolbar_was_visible = flag;			\
	break;								\
      case LEFT_TOOLBAR:						\
	(frame)->left_toolbar_was_visible = flag;			\
	break;								\
      case RIGHT_TOOLBAR:						\
	(frame)->right_toolbar_was_visible = flag;			\
	break;								\
      default:								\
	abort ();							\
      }									\
  } while (0)

static void
x_output_toolbar_button (struct frame *f, Lisp_Object button)
{
    struct device *d = XDEVICE (f->device);
    EmacsFrame ef = (EmacsFrame) FRAME_X_TEXT_WIDGET (f);

    Display *dpy = DEVICE_X_DISPLAY (d);
    Window x_win = XtWindow (FRAME_X_TEXT_WIDGET (f));
    struct toolbar_button *tb = XTOOLBAR_BUTTON (button);

    fprintf (stderr, "x_output_toolbar_button: %p - %p\n", f, tb);
}

static void
x_redraw_exposed_toolbars (struct frame *f, int x, int y, int width,
			   int height)
{
}

static void
x_redraw_frame_toolbars (struct frame *f)
{
}

static void
x_output_frame_toolbars (struct frame *f)
{
}

static void
x_initialize_frame_toolbars (struct frame *f)
{
    GnomacsFrame *frame;
    Lisp_Object button, window;
    struct window *w;

    assert (FRAME_X_P (f));

    frame = XOPAQUE_DATA (FRAME_X_PLUGGABLE_WINDOW_DATA (f));

    assert (frame != NULL);
    assert (GNOME_IS_GNOMACS_FRAME (frame));

    fprintf (stderr, "x_output_frame_toolbars: %p - %p - %d - %d\n",
	     f, frame, FRAME_REAL_TOP_TOOLBAR_VISIBLE (f),
	     f->top_toolbar_was_visible);

    window = FRAME_LAST_NONMINIBUF_WINDOW (f);
    button = FRAME_TOOLBAR_BUTTONS (f, TOP_TOOLBAR);

    w = XWINDOW (window);

    while (!NILP (button)) {
	struct toolbar_button *tb = XTOOLBAR_BUTTON (button);
	Lisp_Object glyph, instance;
	Lisp_Image_Instance *p;

	fprintf (stderr, "button: %p\n", tb);

	glyph = get_toolbar_button_glyph (w, tb);
	/* #### It is currently possible for users to trash us by directly
	   changing the toolbar glyphs.  Avoid crashing in that case. */
	if (GLYPHP (glyph))
	    instance = glyph_image_instance (glyph, window, ERROR_ME_NOT, 1);
	else
	    instance = Qnil;

	p = XIMAGE_INSTANCE (instance);

	if (IMAGE_INSTANCE_PIXMAP_TYPE_P (p) && !NILP (p->u.pixmap.filename)) {
	    gnomacs_frame_add_pixmap (frame, XSTRING_DATA (p->u.pixmap.filename));
	}

	button = tb->next;
    }
}

static void
x_free_frame_toolbars (struct frame *f)
{
}


/************************************************************************/
/*                            initialization                            */
/************************************************************************/

void
console_type_create_toolbar_bonobo (void)
{
    CONSOLE_HAS_METHOD (x, output_frame_toolbars);
    CONSOLE_HAS_METHOD (x, initialize_frame_toolbars);
    CONSOLE_HAS_METHOD (x, free_frame_toolbars);
    CONSOLE_HAS_METHOD (x, output_toolbar_button);
    CONSOLE_HAS_METHOD (x, redraw_exposed_toolbars);
    CONSOLE_HAS_METHOD (x, redraw_frame_toolbars);
}
