/*
 * Very simple sample module. Illustrates most of the salient features
 * of Emacs dynamic modules.
 * (C) Copyright 1998, 1999 J. Kean Johnston. All rights reserved.
 */

#include <gnomacs.h>
#include <GnomacsFrame.h>
#include <dlfcn.h>

Lisp_Object Qgnomacs_minor_mode;
Lisp_Object Qgnomacs_init_hook;
Lisp_Object Qgnomacs_visit_hook;
Lisp_Object Qgnomacs_done_hook;

Lisp_Object Qframe_created_hook;

Lisp_Object Qgnomacs_make_frame;

Lisp_Object Qnormal;

/* defined in sysdll.c */
extern struct Lisp_Subr *make_pure_subr (struct Lisp_Subr *subr);

DEFUN ("gnomacs-init", Fgnomacs_init, Sgnomacs_init, 0, 0, 0,
       "Initialize Gnomacs-FSF.")
     ()
{
    static int initialized = 0;

    if (initialized)
	return Qnil;

    fprintf (stderr, "\nInitializing Gnomacs FSF!\n");

    gnomacs_gnome_init ();

    initialized = 1;

    return Qt;
}

DEFUN ("gnomacs-kill-emacs", Fgnomacs_kill_emacs, Sgnomacs_kill_emacs, 0, 0, 0,
       "Call this in kill-emacs-hook to finalize Gnomacs.")
     ()
{
    fprintf (stderr, "Done calling kill emacs\n");

    return Qnil;
}

DEFUN ("gnomacs-frame-created-callback", Fgnomacs_frame_created_callback, Sgnomacs_frame_created_callback, 2, 2, 0,
       "Callback function which is called when the frame is created.")
     (lframe, eframe)
     Lisp_Object lframe, eframe;
{
    GnomacsFrame *frame = (GnomacsFrame *) XUINT (lframe);

    g_return_val_if_fail (frame != NULL, Qnil);
    g_return_val_if_fail (GNOME_IS_GNOMACS_FRAME (frame), Qnil);

    gnomacs_frame_created (frame, GINT_TO_POINTER ((int) eframe));

    return Qnil;
}

DEFUN ("gnomacs-test", Fgnomacs_test, Sgnomacs_test, 0, 0, 0,
       "Test function.")
     ()
{
    return Qnil;
}

static void
syms_of_gnomacs()
{
    defsubr (make_pure_subr (&Sgnomacs_init));
    defsubr (make_pure_subr (&Sgnomacs_kill_emacs));
    defsubr (make_pure_subr (&Sgnomacs_test));
    defsubr (make_pure_subr (&Sgnomacs_frame_created_callback));

    staticpro (&Qgnomacs_minor_mode);
    Qgnomacs_minor_mode = intern ("gnomacs-minor-mode");
    staticpro (&Qgnomacs_init_hook);
    Qgnomacs_init_hook = intern ("gnomacs-init-hook");
    staticpro (&Qgnomacs_visit_hook);
    Qgnomacs_visit_hook = intern ("gnomacs-visit-hook");
    staticpro (&Qgnomacs_done_hook);
    Qgnomacs_done_hook = intern ("gnomacs-done-hook");

    staticpro (&Qframe_created_hook);
    Qframe_created_hook = intern ("frame-created-hook");

    staticpro (&Qgnomacs_make_frame);
    Qgnomacs_make_frame = intern ("gnomacs-make-frame");

    staticpro (&Qnormal);
    Qnormal = intern ("normal");
}

void
gnomacs_fsf_init (void)
{
    syms_of_gnomacs ();
}

void
gnomacs_set_buffer (Lisp_Object buffer)
{
    Fset_buffer (buffer);
}

void
gnomacs_insert_file_contents (Lisp_Object buffer, Lisp_Object filename)
{
}

void
gnomacs_erase_buffer (Lisp_Object buffer)
{
    if (!NILP (buffer)) {
	Lisp_Object current_buffer;

	current_buffer = Fcurrent_buffer ();
	Fset_buffer (buffer);
	Ferase_buffer ();
	Fset_buffer (current_buffer);
    } else {
	Ferase_buffer ();
    }
}
