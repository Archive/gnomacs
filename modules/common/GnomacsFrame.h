#ifndef __GNOMACS_FRAME_H__
#define __GNOMACS_FRAME_H__

#include <gnome.h>
#include <libgnorba/gnorba.h>

BEGIN_GNOME_DECLS

#define GNOMACS_FRAME(obj)		GTK_CHECK_CAST (obj, gnomacs_frame_get_type (), GnomacsFrame)
#define GNOMACS_FRAME_CLASS(klass)	GTK_CHECK_CLASS_CAST (klass, gnomacs_frame_get_type (), GnomacsFrameClass)
#define GNOME_IS_GNOMACS_FRAME(obj)	GTK_CHECK_TYPE (obj, gnomacs_frame_get_type ())


typedef struct _GnomacsFrame		GnomacsFrame;
typedef struct _GnomacsFramePrivate	GnomacsFramePrivate;
typedef struct _GnomacsFrameClass	GnomacsFrameClass;

typedef enum _GnomacsFrameType		GnomacsFrameType;

typedef struct _gnomacs_emacs_frame_t	gnomacs_emacs_frame_t;

enum _GnomacsFrameType
{
    GNOMACS_FRAME_NORMAL,
    GNOMACS_FRAME_MINIBUFFER
};

struct _GnomacsFrame
{
    GtkSocket socket;

    GnomacsFramePrivate *_priv;
};

struct _GnomacsFrameClass
{
    GtkSocketClass parent_class;

    void (*frame_created)		(gnomacs_emacs_frame_t *);
    void (*emacs_killed)		(void);
};

struct _gnomacs_emacs_frame_t
{
    GnomacsFrameType type;

    Lisp_Object frame, frame_type;
    struct gcpro gcpro1;
};

GtkWidget*
gnomacs_frame_new			(GnomacsFrameType);

guint
gnomacs_frame_get_type			(void);

void
gnomacs_frame_set_minibuffer		(GnomacsFrame *, GnomacsFrame *);

void
gnomacs_frame_set_ui_handler		(GnomacsFrame *, BonoboUIHandler *);

void
gnomacs_frame_ready			(GnomacsFrame *);

void
gnomacs_frame_created			(GnomacsFrame *, gpointer);

void
gnomacs_frame_set_char_size		(GnomacsFrame *, guint, guint);

void
gnomacs_frame_set_toolbar_visible	(GnomacsFrame *, gboolean);

void
gnomacs_frame_set_gutter_visible	(GnomacsFrame *, gboolean);

void
gnomacs_frame_set_menubar_visible	(GnomacsFrame *, gboolean);

void
gnomacs_frame_set_unsplittable		(GnomacsFrame *, gboolean);

void
gnomacs_frame_add_pixmap		(GnomacsFrame *, const char *);

END_GNOME_DECLS

#endif /* __GNOMACS_FRAME_H__ */
