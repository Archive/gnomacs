#ifndef __GNOMACS_H__
#define __GNOMACS_H__ 1

#ifdef GNOMACS_FSF
/* GNU Emacs */
#include <emacs-config.h>
#include <lisp.h>
#include <frame.h>
#else
/* XEmacs */
#include <emodules.h>
#endif

#include <gnome.h>
#include <liboaf/liboaf.h>
#include <bonobo.h>

BEGIN_GNOME_DECLS

typedef struct _GnomacsFunctions		GnomacsFunctions;

struct _GnomacsFunctions
{
    void (*init_func) (void);
    void (*idle_loop) (void);
};

void
gnomacs_gnome_init (void);

extern void
gnomacs_gnome_idle_loop (void);

extern void
gnomacs_bonobo_init (void);

extern void
gnomacs_fsf_init (void);

extern void
gnomacs_set_buffer (Lisp_Object);

extern void
gnomacs_erase_buffer (Lisp_Object);

extern void
gnomacs_insert_file_contents (Lisp_Object, Lisp_Object);

/* Symbols. */

extern Lisp_Object Qgnomacs_minor_mode;
extern Lisp_Object Qgnomacs_init_hook;
extern Lisp_Object Qgnomacs_visit_hook;
extern Lisp_Object Qgnomacs_done_hook;

extern Lisp_Object Qframe_created_hook;

extern Lisp_Object Qgnomacs_make_frame;

extern Lisp_Object Qnormal;

/* External declarations. */

extern Lisp_Object Qonly;
extern Lisp_Object Qminibuffer;
extern Lisp_Object Qunsplittable;
extern Lisp_Object Qset_specifier;
extern Lisp_Object Vdefault_toolbar_visible_p;
extern Lisp_Object Vdefault_gutter_visible_p;
extern Lisp_Object Vmenubar_visible_p;

/* Garbage collecting stuff */

#ifdef DEBUG_GCPRO

#define MGCPRO1(s,v) \
 debug_gcpro1 (__FILE__, __LINE__,&(s->gcpro1),&v)
#define MGCPRO2(s,v1,v2) \
 debug_gcpro2 (__FILE__, __LINE__,&(s->gcpro1),&(s->gcpro2),&v1,&v2)
#define MGCPRO3(s,v1,v2,v3) \
 debug_gcpro3 (__FILE__, __LINE__,&(s->gcpro1),&(s->gcpro2),\
	       &(s->gcpro3),&v1,&v2,&v3)
#define MGCPRO4(s,v1,v2,v3,v4) \
 debug_gcpro4 (__FILE__, __LINE__,&(s->gcpro1),&(s->gcpro2),\
	       &(s->gcpro3),&(s->gcpro4),&v1,&v2,&v3,&v4)
#define MGCPRO5(s,v1,v2,v3,v4,v5) \
 debug_gcpro5 (__FILE__, __LINE__,&(s->gcpro1),&(s->gcpro2),\
	       &(s->gcpro3),&(s->gcpro4,&(s->gcpro5),\
	       &v1,&v2,&v3,&v4,&v5)
#define MUNGCPRO(s) \
 debug_ungcpro(__FILE__, __LINE__,&(s->gcpro1))

#else /* ! DEBUG_GCPRO */

#define MGCPRO1(s,var1) ((void) (						\
  s->gcpro1.next = gcprolist, s->gcpro1.var = &var1, s->gcpro1.nvars = 1,	\
  gcprolist = &(s->gcpro1) ))

#define MGCPRO2(s, var1, var2) ((void) (					\
  s->gcpro1.next = gcprolist, s->gcpro1.var = &var1, s->gcpro1.nvars = 1,	\
  s->gcpro2.next = &s->gcpro1,   s->gcpro2.var = &var2, s->gcpro2.nvars = 1,	\
  gcprolist = &(s->gcpro2) ))

#define MGCPRO3(s, var1, var2, var3) ((void) (				\
  s->gcpro1.next = gcprolist, s->gcpro1.var = &var1, s->gcpro1.nvars = 1,	\
  s->gcpro2.next = &s->gcpro1,   s->gcpro2.var = &var2, s->gcpro2.nvars = 1,	\
  s->gcpro3.next = &s->gcpro2,   s->gcpro3.var = &var3, s->gcpro3.nvars = 1,	\
  gcprolist = &(s->gcpro3) ))

#define MGCPRO4(s, var1, var2, var3, var4) ((void) (			\
  s->gcpro1.next = gcprolist, s->gcpro1.var = &var1, s->gcpro1.nvars = 1,	\
  s->gcpro2.next = &s->gcpro1,   s->gcpro2.var = &var2, s->gcpro2.nvars = 1,	\
  s->gcpro3.next = &s->gcpro2,   s->gcpro3.var = &var3, s->gcpro3.nvars = 1,	\
  s->gcpro4.next = &s->gcpro3,   s->gcpro4.var = &var4, s->gcpro4.nvars = 1,	\
  gcprolist = &(s->gcpro4) ))

#define MGCPRO5(s, var1, var2, var3, var4, var5) ((void) (			\
  s->gcpro1.next = gcprolist, s->gcpro1.var = &var1, s->gcpro1.nvars = 1,	\
  s->gcpro2.next = &s->gcpro1,   s->gcpro2.var = &var2, s->gcpro2.nvars = 1,	\
  s->gcpro3.next = &s->gcpro2,   s->gcpro3.var = &var3, s->gcpro3.nvars = 1,	\
  s->gcpro4.next = &s->gcpro3,   s->gcpro4.var = &var4, s->gcpro4.nvars = 1,	\
  s->gcpro5.next = &s->gcpro4,   s->gcpro5.var = &var5, s->gcpro5.nvars = 1,	\
  gcprolist = &(s->gcpro5) ))

#define MUNGCPRO(s) ((void) (gcprolist = s->gcpro1.next))

#endif /* ! DEBUG_GCPRO */

END_GNOME_DECLS

#endif
