#include <gnomacs.h>

static CORBA_ORB gnomacs_orb;
static gboolean gnomacs_initialized = FALSE;

void
gnomacs_gnome_idle_loop (void)
{
    gtk_main_iteration_do (FALSE);
}

void
gnomacs_gnome_init (void)
{
    char *argv [] = { "gnomacs-gnome", NULL };
    int argc = 1;

    if (gnomacs_initialized) {
	fprintf (stderr, "gnomacs already initialized!\n");
	return;
    }

    fprintf (stderr, "gnomacs_gnome_init!\n");

#ifndef GNOMACS_FSF
    gnome_init_with_popt_table ("gnomacs_gnome", "0.01", argc, argv,
				oaf_popt_options, 0, NULL);
#endif
    gnomacs_orb = oaf_init (argc, argv);

    if (bonobo_init (gnomacs_orb, NULL, NULL) == FALSE) {
	fprintf (stderr, "gnomacs_bonobo_init failed!\n");
	return;
    }

    gnomacs_bonobo_init ();

    fprintf (stderr, "gnome_init done!\n");

    gnomacs_initialized = TRUE;
}

GnomacsFunctions GnomacsFunctionTable = {
    &gnomacs_gnome_init,
    &gnomacs_gnome_idle_loop
};
