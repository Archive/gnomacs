#include <gnomacs.h>
#include <GnomacsFrame.h>
#include <gdk/gdkx.h>

#include <X11/Intrinsic.h>

#ifdef GNOMACS_XEMACS
#include <console-x.h>
#endif

enum {
    FRAME_CREATED,
    EMACS_KILLED,
    LAST_SIGNAL
};

struct _GnomacsFramePrivate
{
    GnomacsFrameType type;

    GnomacsFrame *minibuffer;

    BonoboUIHandler *uih;

    gnomacs_emacs_frame_t *emacs_frame;
};

static gint gnomacs_frame_signals [LAST_SIGNAL];

/* Forward declararations */

static void
gnomacs_frame_class_init	(GnomacsFrameClass	*klass);

static void
gnomacs_frame_init		(GnomacsFrame		*frame);

static void
gnomacs_frame_realize		(GtkWidget		*widget);

static void
gnomacs_frame_size_request	(GtkWidget		*widget,
				 GtkRequisition		*requisition);

static void
gnomacs_frame_size_allocate	(GtkWidget		*widget,
				 GtkAllocation		*allocation);

static GdkFilterReturn
gnomacs_frame_filter_func	(GdkXEvent		*gdk_xevent, 
				 GdkEvent		*event, 
				 gpointer		data);

static GtkSocketClass *parent_class = NULL;

guint
gnomacs_frame_get_type (void)
{
    static guint gnomacs_frame_type = 0;

    if (!gnomacs_frame_type) {
	static const GtkTypeInfo gnomacs_frame_info = {
	    "GnomacsFrame",
	    sizeof (GnomacsFrame),
	    sizeof (GnomacsFrameClass),
	    (GtkClassInitFunc) gnomacs_frame_class_init,
	    (GtkObjectInitFunc) gnomacs_frame_init,
	    (GtkArgSetFunc) NULL,
	    (GtkArgGetFunc) NULL
	};

	gnomacs_frame_type = gtk_type_unique
	    (gtk_socket_get_type (), &gnomacs_frame_info);
    }

    return gnomacs_frame_type;
}

static void
gnomacs_frame_class_init (GnomacsFrameClass *klass)
{
    GtkObjectClass *object_class;
    GtkWidgetClass *widget_class;
    GtkSocketClass *socket_class;

    object_class = (GtkObjectClass*) klass;
    widget_class = (GtkWidgetClass*) klass;
    socket_class = (GtkSocketClass*) klass;

    gnomacs_frame_signals [FRAME_CREATED] =
	gtk_signal_new ("frame_created",
			GTK_RUN_LAST,
			object_class->type,
			GTK_SIGNAL_OFFSET
			(GnomacsFrameClass, frame_created),
			gtk_marshal_NONE__POINTER,
			GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

    gnomacs_frame_signals [EMACS_KILLED] =
	gtk_signal_new ("emacs_killed",
			GTK_RUN_LAST,
			object_class->type,
			GTK_SIGNAL_OFFSET
			(GnomacsFrameClass, emacs_killed),
			gtk_marshal_NONE__NONE,
			GTK_TYPE_NONE, 0);

    gtk_object_class_add_signals
	(object_class, gnomacs_frame_signals, LAST_SIGNAL);

    widget_class->realize = gnomacs_frame_realize;
    widget_class->size_request = gnomacs_frame_size_request;
    widget_class->size_allocate = gnomacs_frame_size_allocate;

    parent_class = gtk_type_class (gtk_socket_get_type ());
}

static void
gnomacs_frame_init (GnomacsFrame *frame)
{
    frame->_priv = g_new0 (GnomacsFramePrivate, 1);
}

GtkWidget*
gnomacs_frame_new (GnomacsFrameType type)
{
    GnomacsFrame *frame;

    frame = gtk_type_new (gnomacs_frame_get_type ());

    frame->_priv->type = type;

    return GTK_WIDGET (frame);
}

static Lisp_Object
gnomacs_make_frame (GnomacsFrame *frame, long xid)
{
    Lisp_Object lisp_xid, xid_string, retval;
    Lisp_Object lisp_minibuffer = Qnil, lisp_extra_data = Qnil;
    GnomacsFrame *minibuffer;

    XSETINT (lisp_xid, xid);
    xid_string = Fnumber_to_string (lisp_xid);
    XSETINT (lisp_extra_data, (int) frame);

    switch (frame->_priv->type) {
    case GNOMACS_FRAME_NORMAL:
	minibuffer = frame->_priv->minibuffer;
	if (minibuffer && minibuffer->_priv->emacs_frame) {
	    struct frame *f = XFRAME (minibuffer->_priv->emacs_frame->frame);

	    lisp_minibuffer = FRAME_ROOT_WINDOW (f);
	}
	break;
    case GNOMACS_FRAME_MINIBUFFER:
	lisp_minibuffer = Qonly;
	break;
    }

    retval = call3 (Qgnomacs_make_frame,
		    xid_string, lisp_minibuffer,
		    lisp_extra_data);

    return retval;
}

void
gnomacs_frame_created (GnomacsFrame *frame, gpointer eframe)
{
    gnomacs_emacs_frame_t *emacs_frame;
    Lisp_Object args[3];
    struct gcpro gcpro1;
    GtkWidget *widget;

    g_return_if_fail (frame != NULL);
    g_return_if_fail (GNOME_IS_GNOMACS_FRAME (frame));

    widget = GTK_WIDGET (frame);

    fprintf (stderr, "gnomacs_frame_created!\n");

    emacs_frame = g_new0 (gnomacs_emacs_frame_t, 1);

    emacs_frame->frame = (Lisp_Object) GPOINTER_TO_INT (eframe);
    MGCPRO1 (emacs_frame, emacs_frame->frame);

    switch (frame->_priv->type) {
    case GNOMACS_FRAME_NORMAL:
	emacs_frame->frame_type = Qnormal;
	break;
    case GNOMACS_FRAME_MINIBUFFER:
	emacs_frame->frame_type = Qminibuffer;
	break;
    default:
	emacs_frame->frame_type = Qnil;
	g_assert_not_reached ();
    }

    gdk_window_add_filter (widget->window, gnomacs_frame_filter_func, widget);

    frame->_priv->emacs_frame = emacs_frame;

    args[0] = Qframe_created_hook;
    args[1] = emacs_frame->frame;
    args[2] = emacs_frame->frame_type;
    GCPRO1 (args[0]);
    gcpro1.nvars = 3;
    Frun_hook_with_args (2, args);
    UNGCPRO;

    gtk_signal_emit_by_name (GTK_OBJECT (widget), "frame_created", emacs_frame);
}

void
gnomacs_frame_ready (GnomacsFrame *frame)
{
    Lisp_Object retval;
    GtkWidget *widget;
    long xid;

    g_return_if_fail (frame != NULL);
    g_return_if_fail (GNOME_IS_GNOMACS_FRAME (frame));

    gdk_flush ();

    g_return_if_fail (frame->_priv->emacs_frame == NULL);

    if (frame->_priv->minibuffer)
	g_return_if_fail (frame->_priv->minibuffer->_priv->emacs_frame != NULL);

    widget = GTK_WIDGET (frame);
    xid = GDK_WINDOW_XWINDOW (widget->window);

    fprintf (stderr, "gnomacs_frame_realize - external window: %ld\n", xid);

    retval = gnomacs_make_frame (frame, xid);

#ifdef GNOMACS_XEMACS
    gnomacs_frame_created (frame, GINT_TO_POINTER ((int) retval));
#endif
}

static void
gnomacs_frame_size_request (GtkWidget      *widget,
			    GtkRequisition *requisition)
{
    g_return_if_fail (widget != NULL);
    g_return_if_fail (GNOME_IS_GNOMACS_FRAME (widget));
    g_return_if_fail (requisition != NULL);

    if (GTK_WIDGET_CLASS (parent_class)->size_request)
	(* GTK_WIDGET_CLASS (parent_class)->size_request) (widget, requisition);

    fprintf (stderr, "gnomacs_frame_size_request: (%d,%d)\n",
	     requisition->width, requisition->height);
}

static void
gnomacs_frame_size_allocate (GtkWidget     *widget,
			     GtkAllocation *allocation)
{
    GnomacsFrame *frame;

    g_return_if_fail (widget != NULL);
    g_return_if_fail (GNOME_IS_GNOMACS_FRAME (widget));
    g_return_if_fail (allocation != NULL);

    frame = GNOMACS_FRAME (widget);

    fprintf (stderr, "gnomacs_frame_size_allocate: (%d,%d) - (%d,%d)\n",
	     allocation->x, allocation->y, allocation->width,
	     allocation->height);

    if (GTK_WIDGET_CLASS (parent_class)->size_allocate)
	(* GTK_WIDGET_CLASS (parent_class)->size_allocate) (widget, allocation);
}


static void
gnomacs_frame_realize (GtkWidget *widget)
{
    g_return_if_fail (widget != NULL);
    g_return_if_fail (GNOME_IS_GNOMACS_FRAME (widget));

    if (GTK_WIDGET_CLASS (parent_class)->realize)
	(* GTK_WIDGET_CLASS (parent_class)->realize) (widget);
}

static GdkFilterReturn
gnomacs_frame_filter_func (GdkXEvent *gdk_xevent, GdkEvent *event, gpointer data)
{
    GnomacsFrame *frame;
    GtkWidget *widget;
    XEvent *xevent;

    GdkFilterReturn return_val;

    frame = GNOMACS_FRAME (data);
    widget = GTK_WIDGET (frame);
    xevent = (XEvent *)gdk_xevent;

    return_val = GDK_FILTER_CONTINUE;

    return return_val;
}

void
gnomacs_frame_set_minibuffer (GnomacsFrame *frame, GnomacsFrame *minibuffer)
{
    g_return_if_fail (frame != NULL);
    g_return_if_fail (GNOME_IS_GNOMACS_FRAME (frame));
    g_return_if_fail (frame->_priv->type == GNOMACS_FRAME_NORMAL);

    g_return_if_fail (minibuffer != NULL);
    g_return_if_fail (GNOME_IS_GNOMACS_FRAME (minibuffer));
    g_return_if_fail (minibuffer->_priv->type == GNOMACS_FRAME_MINIBUFFER);

    g_return_if_fail (frame->_priv->minibuffer == NULL);

    frame->_priv->minibuffer = minibuffer;
}

void
gnomacs_frame_set_ui_handler (GnomacsFrame *frame, BonoboUIHandler *uih)
{
    g_return_if_fail (frame != NULL);
    g_return_if_fail (GNOME_IS_GNOMACS_FRAME (frame));

    g_return_if_fail (uih != NULL);
    g_return_if_fail (BONOBO_IS_UI_HANDLER (uih));

    g_return_if_fail (frame->_priv->uih == NULL);

    frame->_priv->uih = uih;
}

void
gnomacs_frame_set_char_size (GnomacsFrame *frame, guint cols, guint rows)
{
    struct frame *f;
#ifdef GNOMACS_XEMACS
    int width, height;
#endif

    g_return_if_fail (frame != NULL);
    g_return_if_fail (GNOME_IS_GNOMACS_FRAME (frame));
    g_return_if_fail (frame->_priv->emacs_frame);

    f = XFRAME (frame->_priv->emacs_frame->frame);

#ifdef GNOMACS_XEMACS
    fprintf (stderr, "OLD SIZE: (%d,%d) - (%d,%d)\n",
	     FRAME_WIDTH (f), FRAME_HEIGHT (f),
	     FRAME_CHARWIDTH (f), FRAME_CHARHEIGHT (f));

    EmacsFrameSetCharSize (FRAME_X_TEXT_WIDGET (f), cols, rows);

    fprintf (stderr, "NEW SIZE: (%d,%d) - (%d,%d)\n",
	     FRAME_WIDTH (f), FRAME_HEIGHT (f),
	     FRAME_CHARWIDTH (f), FRAME_CHARHEIGHT (f));

    change_frame_size (f, rows, cols, 0);

    fprintf (stderr, "NEW SIZE: (%d,%d) - (%d,%d)\n",
	     FRAME_WIDTH (f), FRAME_HEIGHT (f),
	     FRAME_CHARWIDTH (f), FRAME_CHARHEIGHT (f));

    char_to_pixel_size (f, FRAME_CHARWIDTH (f), FRAME_CHARHEIGHT (f),
			&width, &height);

    fprintf (stderr, "NEW PIXEL SIZE: (%d,%d)\n", width, height);

    gtk_widget_set_usize (GTK_WIDGET (frame), width, height);
    gtk_widget_queue_resize (GTK_WIDGET (frame));
#endif
}

void
gnomacs_frame_set_toolbar_visible (GnomacsFrame *frame, gboolean visible)
{
    g_return_if_fail (frame != NULL);
    g_return_if_fail (GNOME_IS_GNOMACS_FRAME (frame));
    g_return_if_fail (frame->_priv->type == GNOMACS_FRAME_NORMAL);
    g_return_if_fail (frame->_priv->emacs_frame);

#ifdef GNOMACS_XEMACS
    call3 (Qset_specifier, Vdefault_toolbar_visible_p,
	   visible ? Qt : Qnil, frame->_priv->emacs_frame->frame);
#endif
}

void
gnomacs_frame_set_gutter_visible (GnomacsFrame *frame, gboolean visible)
{
    g_return_if_fail (frame != NULL);
    g_return_if_fail (GNOME_IS_GNOMACS_FRAME (frame));
    g_return_if_fail (frame->_priv->type == GNOMACS_FRAME_NORMAL);
    g_return_if_fail (frame->_priv->emacs_frame);

#ifdef GNOMACS_XEMACS
    call3 (Qset_specifier, Vdefault_gutter_visible_p,
	   visible ? Qt : Qnil, frame->_priv->emacs_frame->frame);
#endif
}

void
gnomacs_frame_set_menubar_visible (GnomacsFrame *frame, gboolean visible)
{
#ifdef GNOMACS_XEMACS
    struct frame *f;
#endif

    g_return_if_fail (frame != NULL);
    g_return_if_fail (GNOME_IS_GNOMACS_FRAME (frame));
    g_return_if_fail (frame->_priv->type == GNOMACS_FRAME_NORMAL);
    g_return_if_fail (frame->_priv->emacs_frame);

#ifdef GNOMACS_XEMACS
    call3 (Qset_specifier, Vmenubar_visible_p,
	   visible ? Qt : Qnil, frame->_priv->emacs_frame->frame);

    f = XFRAME (frame->_priv->emacs_frame->frame);

    MARK_FRAME_SIZE_SLIPPED (f);
    adjust_frame_size (f);
#endif
}

void
gnomacs_frame_set_unsplittable (GnomacsFrame *frame, gboolean unsplittable)
{
#ifdef GNOMACS_XEMACS
    Lisp_Object plist;
#else
    Lisp_Object alist;
#endif
    struct gcpro gcpro1;

    g_return_if_fail (frame != NULL);
    g_return_if_fail (GNOME_IS_GNOMACS_FRAME (frame));
    g_return_if_fail (frame->_priv->type == GNOMACS_FRAME_NORMAL);
    g_return_if_fail (frame->_priv->emacs_frame);

#ifdef GNOMACS_XEMACS
    plist = Qnil;
    external_plist_put (&plist, Qunsplittable, unsplittable ? Qt : Qnil,
			1, ERROR_ME);

    GCPRO1 (plist);
    Fset_frame_properties (frame->_priv->emacs_frame->frame, plist);
    UNGCPRO;
#else
    alist = Qnil;
    store_in_alist (&alist, Qunsplittable, unsplittable ? Qt : Qnil);

    GCPRO1 (alist);
    Fmodify_frame_parameters (frame->_priv->emacs_frame->frame, alist);
    UNGCPRO;
#endif
}

void
gnomacs_frame_add_pixmap (GnomacsFrame *frame, const char *filename)
{
    g_return_if_fail (frame != NULL);
    g_return_if_fail (GNOME_IS_GNOMACS_FRAME (frame));
    g_return_if_fail (frame->_priv->uih != NULL);
    g_return_if_fail (filename != NULL);

    fprintf (stderr, "gnomacs_frame_add_pixmap: %p - '%s'\n", frame, filename);
}
