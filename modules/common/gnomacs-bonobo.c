#include <gnomacs.h>
#include <GnomacsFrame.h>

#include <gdk/gdkx.h>

/* FIXME: Why isn't this is buffer.h ? */
EXFUN (Fgenerate_new_buffer_name, 2);

#ifdef GNOMACS_FSF
#define FACTORY_OAFIID "OAFIID:embeddable-factory:gnomacs-fsf-text-plain:3cfa22fb-711f-4995-bb92-b363ead29731"
#else
#define FACTORY_OAFIID "OAFIID:embeddable-factory:gnomacs-text-plain:e496fca5-0aaf-4678-9aef-4bf81fcf723c"
#endif

/*
 * Number of running objects
 */ 
static int running_objects = 0;

/*
 * Our embeddable factory
 */
static BonoboEmbeddableFactory *factory;

/*
 * BonoboObject data
 */
typedef struct {
    BonoboEmbeddable *bonobo_object;

    /* List of this component's Bonobo::Views. */
    GList *views;

    Lisp_Object emacs_buffer_name;
    Lisp_Object emacs_buffer;
    struct gcpro gcpro1, gcpro2;
} bonobo_object_data_t;

/*
 * View data
 */
typedef struct {
    BonoboView *view;

    bonobo_object_data_t *bonobo_object_data;

    GtkWidget *frame;
#ifdef WANT_MINIBUFFER_FRAME
    GtkWidget *minibuffer_frame;
#endif

    BonoboUIHandler *uih;

#ifdef WANT_MINIBUFFER_FRAME
    BonoboControl *minibuffer_control;
#endif
    BonoboPropertyBag *pb;

    gnomacs_emacs_frame_t *emacs_frame;

    gboolean active;
} view_data_t;

enum {
    PROP_TOOLBAR_VISIBLE,
    PROP_GUTTER_VISIBLE,
    PROP_MENUBAR_VISIBLE,
    PROP_UNSPLITTABLE
} MyProps;

#define TOOLBAR_VISIBLE_KEY	"Gnomacs::toolbar_visible"
#define GUTTER_VISIBLE_KEY	"Gnomacs::gutter_visible"
#define MENUBAR_VISIBLE_KEY	"Gnomacs::menubar_visible"
#define UNSPLITTABLE_KEY	"Gnomacs::unsplittable"

static void
embeddable_system_exception_cb (BonoboEmbeddable *embeddable,
				CORBA_Object corba_object,
				CORBA_Environment *ev,
				gpointer data)
{
    g_warning ("Embeddable system exception");
    bonobo_object_destroy (BONOBO_OBJECT (embeddable));
}

static void
bonobo_object_destroy_cb (BonoboEmbeddable *bonobo_object,
			  bonobo_object_data_t *bonobo_object_data)
{
    running_objects--;
    if (running_objects > 0)
	return;

    if (factory) {
	bonobo_object_unref (BONOBO_OBJECT (factory));
	factory = NULL;
	gtk_main_quit ();
    }
}

/*
 * Bonobo::PersistStream
 *
 * These two functions implement the Bonobo::PersistStream load and
 * save methods which allow data to be loaded into and out of the
 * BonoboObject.
 */
static int
stream_read (Bonobo_Stream stream, bonobo_object_data_t *bonobo_object_data)
{
#ifdef GNOMACS_XEMACS
    Bonobo_Stream_iobuf *buffer;
    CORBA_long bytes_read;
    CORBA_Environment ev;

    CORBA_exception_init (&ev);

    do {
#define READ_CHUNK_SIZE 65536
	struct buffer *b;

	bytes_read = Bonobo_Stream_read (stream, READ_CHUNK_SIZE,
					 &buffer, &ev);

	if (!bytes_read)
	    break;

	b = decode_buffer (bonobo_object_data->emacs_buffer, 0);

	buffer_insert_string (b, buffer->_buffer, Qnil,
			      0, buffer->_length);

	CORBA_free (buffer);
    } while (bytes_read > 0);

    CORBA_exception_free (&ev);

    if (bytes_read < 0)
	return -1;

#endif /* GNOMACS_XEMACS */

    return 0;
} /* stream_read */



/*
 * This function implements the Bonobo::PersistStream:load method.
 */
static int
pstream_load (BonoboPersistStream *ps, Bonobo_Stream stream,
	      void *data)
{
    bonobo_object_data_t *bonobo_object_data = data;

    /*
     * 1. Free the old text data and blank the views.
     */
    gnomacs_erase_buffer (bonobo_object_data->emacs_buffer);

    /*
     * 2. Read the new text data.
     */
    if (stream_read (stream, bonobo_object_data) < 0)
	return -1; /* This will raise an exception. */

    return -1;
} /* pstream_load */

/*
 * This function implements the Bonobo::PersistStream:save method.
 */
static int
pstream_save (BonoboPersistStream *ps, Bonobo_Stream stream,
	      void *data)
{
#ifdef GNOMACS_XEMACS
    bonobo_object_data_t *bonobo_object_data = data;

#endif /* GNOMACS_XEMACS */
    return -1;
} /* pstream_save */

/*
 * This function implements the Bonobo::PersistFile:load method.
 */
static int
pfile_load (BonoboPersistFile *pf, const CORBA_char *filename,
	    void *data)
{
    bonobo_object_data_t *bonobo_object_data = data;

    Lisp_Object file, args[1];
    struct gcpro gcpro1, gcpro2;

    /*
     * 1. Free the old text data and blank the views.
     */
    gnomacs_set_buffer (bonobo_object_data->emacs_buffer);
    gnomacs_erase_buffer (Qnil);

    file = build_string ((char *) filename);
    args[0] = Qgnomacs_visit_hook;
    GCPRO2 (file, args[0]);

    gnomacs_insert_file_contents (bonobo_object_data->emacs_buffer,
				  file);

    Fset (Qgnomacs_minor_mode, Qt);

    Frun_hook_with_args (1, args);

    UNGCPRO;

    return 0;
} /* pfile_load */

/*
 * This function implements the Bonobo::PersistFile:save method.
 */
static int
pfile_save (BonoboPersistFile *pf, const CORBA_char *filename,
	    void *data)
{
    return -1;
} /* pfile_save */


/*
 * BonoboObject construction functions.
 */
static void
frame_created_cb (GtkWidget *widget, gnomacs_emacs_frame_t *emacs_frame,
		  view_data_t *view_data)
{
#ifdef GNOMACS_XEMACS
    fprintf (stderr, "frame_created_cb: %p - %p - %p\n", widget,
	     emacs_frame, view_data);

    view_data->emacs_frame = emacs_frame;

    Fset_buffer (view_data->bonobo_object_data->emacs_buffer);
    Fset_window_buffer (Fselected_window (view_data->emacs_frame->frame),
			view_data->bonobo_object_data->emacs_buffer,
			Qnil);
#endif /* GNOMACS_XEMACS */
}

static void
get_prop (BonoboPropertyBag *bag,
	  BonoboArg         *arg,
	  guint              arg_id,
	  gpointer           user_data)
{
    view_data_t *view_data = user_data;

    switch (arg_id) {
    case PROP_TOOLBAR_VISIBLE:
	{
	    gboolean b = GPOINTER_TO_UINT (gtk_object_get_data (GTK_OBJECT (view_data->frame), TOOLBAR_VISIBLE_KEY));
	    BONOBO_ARG_SET_BOOLEAN (arg, b);
	    break;
	}
    case PROP_GUTTER_VISIBLE:
	{
	    gboolean b = GPOINTER_TO_UINT (gtk_object_get_data (GTK_OBJECT (view_data->frame), GUTTER_VISIBLE_KEY));
	    BONOBO_ARG_SET_BOOLEAN (arg, b);
	    break;
	}
    case PROP_MENUBAR_VISIBLE:
	{
	    gboolean b = GPOINTER_TO_UINT (gtk_object_get_data (GTK_OBJECT (view_data->frame), MENUBAR_VISIBLE_KEY));
	    BONOBO_ARG_SET_BOOLEAN (arg, b);
	    break;
	}
    case PROP_UNSPLITTABLE:
	{
	    gboolean b = GPOINTER_TO_UINT (gtk_object_get_data (GTK_OBJECT (view_data->frame), UNSPLITTABLE_KEY));
	    BONOBO_ARG_SET_BOOLEAN (arg, b);
	    break;
	}

    default:
	g_warning ("Unhandled arg %d\n", arg_id);
	break;
    }
}

static void
set_prop (BonoboPropertyBag *bag,
	  const BonoboArg   *arg,
	  guint              arg_id,
	  gpointer           user_data)
{
    view_data_t *view_data = user_data;

    switch (arg_id) {
    case PROP_TOOLBAR_VISIBLE:
	{
	    guint i;

	    i = BONOBO_ARG_GET_BOOLEAN (arg);

	    gnomacs_frame_set_toolbar_visible
		(GNOMACS_FRAME (view_data->frame), i);

	    gtk_object_set_data (GTK_OBJECT (view_data->frame),
				 TOOLBAR_VISIBLE_KEY,
				 GUINT_TO_POINTER (i));
	    break;
	}
    case PROP_GUTTER_VISIBLE:
	{
	    guint i;

	    i = BONOBO_ARG_GET_BOOLEAN (arg);

	    gnomacs_frame_set_gutter_visible
		(GNOMACS_FRAME (view_data->frame), i);

	    gtk_object_set_data (GTK_OBJECT (view_data->frame),
				 GUTTER_VISIBLE_KEY,
				 GUINT_TO_POINTER (i));
	    break;
	}
    case PROP_MENUBAR_VISIBLE:
	{
	    guint i;

	    i = BONOBO_ARG_GET_BOOLEAN (arg);

	    gnomacs_frame_set_menubar_visible
		(GNOMACS_FRAME (view_data->frame), i);

	    gtk_object_set_data (GTK_OBJECT (view_data->frame),
				 MENUBAR_VISIBLE_KEY,
				 GUINT_TO_POINTER (i));
	    break;
	}
    case PROP_UNSPLITTABLE:
	{
	    guint i;

	    i = BONOBO_ARG_GET_BOOLEAN (arg);

	    gnomacs_frame_set_unsplittable
		(GNOMACS_FRAME (view_data->frame), i);

	    gtk_object_set_data (GTK_OBJECT (view_data->frame),
				 UNSPLITTABLE_KEY,
				 GUINT_TO_POINTER (i));
	    break;
	}
    default:
	g_warning ("Unhandled arg %d\n", arg_id);
	break;
    }
}

static void
realize_cb (GtkWidget *frame, gpointer data)
{
    fprintf (stderr, "ID: %ld\n", GDK_WINDOW_XWINDOW (frame->window));
 
    gnomacs_frame_ready (GNOMACS_FRAME (frame));
}


/*
 * When one of our views is activated, we merge our menus
 * in with our container's menus.
 */
static void
view_create_menus (view_data_t *view_data)
{
    Bonobo_UIHandler remote_uih;
#ifdef WANT_MINIBUFFER_FRAME
    Bonobo_Control corba_control;
#endif

    /*
     * Get our container's UIHandler server.
     */
    remote_uih = bonobo_view_get_remote_ui_handler (view_data->view);

    /*
     * We have to deal gracefully with containers
     * which don't have a UIHandler running.
     */
    if (remote_uih == CORBA_OBJECT_NIL)
	return;

    /*
     * Give our BonoboUIHandler object a reference to the
     * container's UIhandler server.
     */
    bonobo_ui_handler_set_container (view_data->uih, remote_uih);

    /*
     * Now we can add our controls.
     */

#ifdef WANT_MINIBUFFER_FRAME
    corba_control = bonobo_object_corba_objref
	(BONOBO_OBJECT (view_data->minibuffer_control));

    bonobo_ui_handler_dock_add (view_data->uih, "Minibuffer",
				corba_control,
				GNOME_DOCK_ITEM_BEH_NORMAL,
				GNOME_DOCK_BOTTOM, 0, 0, 0);

    gnomacs_frame_ready (GNOMACS_FRAME (view_data->minibuffer_frame));

    gnomacs_frame_set_char_size (GNOMACS_FRAME (view_data->minibuffer_frame),
				 50, 2);
#endif
}

/*
 * When this view is deactivated, we must remove our menu items.
 */
static void
view_remove_menus (view_data_t *view_data)
{
    BonoboView *view = view_data->view;
    BonoboUIHandler *uih;

    uih = bonobo_view_get_ui_handler (view);

    bonobo_ui_handler_unset_container (uih);
}

static void
view_activate_cb (BonoboView *view, gboolean activate, view_data_t *view_data)
{
    /*
     * The ViewFrame has just asked the View (that's us) to be
     * activated or deactivated.  We must reply to the ViewFrame
     * and say whether or not we want our activation state to
     * change.  We are an acquiescent BonoboView, so we just agree
     * with whatever the ViewFrame told us.  Most components
     * should behave this way.
     */

    bonobo_view_activate_notify (view, activate);

    /*
     * If we were just activated, we merge in our menu entries.
     * If we were just deactivated, we remove them.
     */
    if (activate)
	view_create_menus (view_data);
    else
	view_remove_menus (view_data);
}

static BonoboView *
view_factory (BonoboEmbeddable       *bonobo_object,
	      const Bonobo_ViewFrame  view_frame,
	      void                   *data)
{
    bonobo_object_data_t *bonobo_object_data = data;
    BonoboView           *view;
    view_data_t          *view_data;

    fprintf (stderr, "view_factory: %p, %p, %p\n",
	     bonobo_object, view_frame, data);

    /*
     * Create the private view data and the GtkText widget.
     */
    view_data = g_new0 (view_data_t, 1);
    view_data->bonobo_object_data = bonobo_object_data;

#ifdef WANT_MINIBUFFER_FRAME
    view_data->minibuffer_frame = gnomacs_frame_new (GNOMACS_FRAME_MINIBUFFER);

    view_data->minibuffer_control = bonobo_control_new
	(view_data->minibuffer_frame);

    gtk_widget_show (view_data->minibuffer_frame);
#endif

    view_data->frame = gnomacs_frame_new (GNOMACS_FRAME_NORMAL);

    gtk_signal_connect (GTK_OBJECT (view_data->frame), "realize",
			(GtkSignalFunc) realize_cb, NULL);

    gtk_widget_set_usize (view_data->frame, 700, 400);

#ifdef WANT_MINIBUFFER_FRAME
    gnomacs_frame_set_minibuffer (GNOMACS_FRAME (view_data->frame),
				  GNOMACS_FRAME (view_data->minibuffer_frame));
#endif

    gtk_signal_connect (GTK_OBJECT (view_data->frame),
			"frame_created", (GtkSignalFunc) frame_created_cb,
			view_data);

    gtk_widget_show_all (view_data->frame);

    /*
     * Now create the BonoboView object.
     */
    view = bonobo_view_new (view_data->frame);
    view_data->view = view;

    /*
     * Grab our BonoboUIHandler object.
     */
    view_data->uih = bonobo_view_get_ui_handler (view_data->view);

    gnomacs_frame_set_ui_handler (GNOMACS_FRAME (view_data->frame),
				  view_data->uih);

    /*
     * When our container wants to activate a given view of this
     * component, we will get the "activate" signal.
     */
    gtk_signal_connect (GTK_OBJECT (view), "activate",
			GTK_SIGNAL_FUNC (view_activate_cb), view_data);

    bonobo_object_data->views = g_list_prepend (bonobo_object_data->views,
						view_data);

    /* Create the properties. */
    view_data->pb = bonobo_property_bag_new (get_prop, set_prop, view_data);
    bonobo_control_set_property_bag (BONOBO_CONTROL (view_data->view),
				     view_data->pb);

    bonobo_property_bag_add (view_data->pb, "toolbar_visible",
			     PROP_TOOLBAR_VISIBLE, BONOBO_ARG_BOOLEAN, NULL,
			     "Whether or not the toolbar is visible", 0);

    bonobo_property_bag_add (view_data->pb, "gutter_visible",
			     PROP_GUTTER_VISIBLE, BONOBO_ARG_BOOLEAN, NULL,
			     "Whether or not the gutter is visible", 0);

    bonobo_property_bag_add (view_data->pb, "menubar_visible",
			     PROP_MENUBAR_VISIBLE, BONOBO_ARG_BOOLEAN, NULL,
			     "Whether or not the menubar is visible", 0);

    bonobo_property_bag_add (view_data->pb, "unsplittable",
			     PROP_UNSPLITTABLE, BONOBO_ARG_BOOLEAN, NULL,
			     "Whether or not the frame is unsplittable", 0);

    return view;
} /* view_factory */

static BonoboObject *
embeddable_factory (BonoboEmbeddableFactory *this, void *data)
{
    BonoboEmbeddable *bonobo_object;
    BonoboPersistStream *stream;
    BonoboPersistFile *file;
    bonobo_object_data_t *bonobo_object_data;

    bonobo_object_data = g_new0 (bonobo_object_data_t, 1);
    if (!bonobo_object_data)
	return NULL;

    /*
     * Creates the BonoboObject server
     */
    bonobo_object = bonobo_embeddable_new (view_factory, bonobo_object_data);
    if (bonobo_object == NULL) {
	g_free (bonobo_object_data);
	return NULL;
    }
    running_objects++;

    fprintf (stderr, "embeddable_factory: %p - %p - %p\n",
	     this, data, bonobo_object);

    gtk_signal_connect_after (GTK_OBJECT (bonobo_object), "destroy",
			      GTK_SIGNAL_FUNC (bonobo_object_destroy_cb),
			      bonobo_object_data);

    gtk_signal_connect (GTK_OBJECT (bonobo_object), "system_exception",
			GTK_SIGNAL_FUNC (embeddable_system_exception_cb),
			bonobo_object_data);

    bonobo_object_data->bonobo_object = bonobo_object;

    bonobo_object_data->emacs_buffer_name =
	Fgenerate_new_buffer_name (build_string ("Gnomacs"), Qnil);
    bonobo_object_data->emacs_buffer =
	Fget_buffer_create (bonobo_object_data->emacs_buffer_name);

    MGCPRO2 (bonobo_object_data,
	     bonobo_object_data->emacs_buffer_name,
	     bonobo_object_data->emacs_buffer);

    /*
     * Register the Bonobo::PersistStream interface.
     */
    stream = bonobo_persist_stream_new (pstream_load,
					pstream_save,
					bonobo_object_data);
    if (stream == NULL) {
	gtk_object_unref (GTK_OBJECT (bonobo_object));
	g_free (bonobo_object_data);
	return NULL;
    }

    bonobo_object_add_interface (BONOBO_OBJECT (bonobo_object),
				 BONOBO_OBJECT (stream));

    /*
     * Register the Bonobo::PersistFile interface.
     */
    file = bonobo_persist_file_new (pfile_load,
				    pfile_save,
				    bonobo_object_data);
    if (file == NULL) {
	gtk_object_unref (GTK_OBJECT (bonobo_object));
	g_free (bonobo_object_data);
	return NULL;
    }

    bonobo_object_add_interface (BONOBO_OBJECT (bonobo_object),
				 BONOBO_OBJECT (file));

    return BONOBO_OBJECT (bonobo_object);
} /* embeddable_factory */

static void
init_bonobo_gnomacs_text_plain_factory (void)
{
    factory = bonobo_embeddable_factory_new (FACTORY_OAFIID,
					     embeddable_factory, NULL);
} /* init_bonobo_text_plain_factory */

void
gnomacs_bonobo_init (void)
{
    init_bonobo_gnomacs_text_plain_factory ();

    bonobo_activate ();
}
