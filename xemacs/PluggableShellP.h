/* Pluggable shell widget internal header file.
   Copyright (C) 1993, 1994 Sun Microsystems, Inc.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Library General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Library General Public License for more details.

You should have received a copy of the GNU Library General Public
License along with this library; if not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA. */

/* Synched up with: Not in FSF. */

/* Written by Ben Wing, September 1993. */

#ifndef INCLUDED_PluggableShellP_h_
#define INCLUDED_PluggableShellP_h_

#include "xintrinsic.h"
#include <X11/ShellP.h>
#include "PluggableShell.h"

typedef struct {		/* new fields for PluggableShell class */
   int dummy;
} PluggableShellClassPart;

typedef struct _PluggableShellClassRec {	/* full class record declaration */
    CoreClassPart core_class;
    CompositeClassPart composite_class;
    ShellClassPart shell_class;
    PluggableShellClassPart pluggableShell_class;
} PluggableShellClassRec;

typedef struct {		/* new fields for PluggableShell widget */
    Window pluggable_window;	/* an already-created window to run on */
    Bool dead_client;		/* is the client dead? */
    unsigned long client_timeout;/* how long to wait for client's response */

    /* private */
    unsigned char client_type;
} PluggableShellPart;

typedef struct _PluggableShellRec {	/* full instance record */
    CorePart core;
    CompositePart composite;
    ShellPart shell;
    PluggableShellPart pluggableShell;
} PluggableShellRec;

extern PluggableShellClassRec pluggableShellClassRec;	 /* class pointer */

#endif /* INCLUDED_PluggableShellP_h_ */
