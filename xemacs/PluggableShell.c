/* Pluggable shell widget.
   Copyright (C) 1993, 1994 Sun Microsystems, Inc.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Library General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Library General Public License for more details.

You should have received a copy of the GNU Library General Public
License along with this library; if not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA. */

/* Synched up with: Not in FSF. */

/* Written by Ben Wing, September 1993. */

/* This is a special Shell that is designed to use an externally-
   provided window created by someone else (possibly another process).
   That other window should have an associated widget of class
   ExternalClient.  The two widgets communicate with each other using
   ClientMessage events and properties on the external window.

   Ideally this feature should be independent of Emacs.  Unfortunately
   there are lots and lots of specifics that need to be dealt with
   for this to work properly, and some of them can't conveniently
   be handled within the widget's methods.  Some day the code may
   be rewritten so that the embedded-widget feature can be used by
   any application, with appropriate entry points that are called
   at specific points within the application.

   This feature is similar to the OLE (Object Linking & Embedding)
   feature provided by MS Windows.
 */

#ifdef emacs

#include <config.h>

#ifndef PLUGGABLE_WIDGET
ERROR!  This ought not be getting compiled if PLUGGABLE_WIDGET is undefined
#endif

#endif /* emacs */

#include <stdio.h>
#include <string.h>
#include <X11/StringDefs.h>
#include "xintrinsicp.h"
#include <X11/Shell.h>
#include <X11/ShellP.h>
#include <X11/Vendor.h>
#include <X11/VendorP.h>
#include "PluggableShellP.h"

#ifdef emacs
extern void emacs_Xt_handle_focus_event (XEvent *event);
#endif

static void PluggableShellInitialize (Widget req, Widget new, ArgList args,
				     Cardinal *num_args);
static void PluggableShellRealize (Widget wid, Mask *vmask, XSetWindowAttributes
				  *attr);
static void PluggableShellDestroy (Widget w);
static void ChangeManaged (Widget wid);
static XtGeometryResult PluggableShellRootGeometryManager (Widget gw,
  XtWidgetGeometry *request, XtWidgetGeometry *reply);
static void EventHandler (Widget wid, XtPointer closure, XEvent *event,
			  Boolean *continue_to_dispatch);

#ifndef DEFAULT_WM_TIMEOUT
# define DEFAULT_WM_TIMEOUT 5000
#endif

void PluggableShellUnrealize (Widget w);

static XtResource resources[] = {
#define offset(field) XtOffset(PluggableShellWidget, pluggableShell.field)
  { XtNwindow, XtCWindow,
    XtRWindow, sizeof (Window),
    offset (pluggable_window), XtRImmediate, (XtPointer)0 },
  { XtNclientTimeout, XtCClientTimeout,
    XtRInt, sizeof (int),
    offset(client_timeout), XtRImmediate,(XtPointer)DEFAULT_WM_TIMEOUT },
  { XtNdeadClient, XtCDeadClient,
    XtRBoolean, sizeof (Boolean),
    offset(dead_client), XtRImmediate, (XtPointer)False },
};

static CompositeClassExtensionRec compositeClassExtRec = {
    NULL,
    NULLQUARK,
    XtCompositeExtensionVersion,
    sizeof (CompositeClassExtensionRec),
    TRUE,
};

static ShellClassExtensionRec shellClassExtRec = {
    NULL,
    NULLQUARK,
    XtShellExtensionVersion,
    sizeof (ShellClassExtensionRec),
    PluggableShellRootGeometryManager
};

PluggableShellClassRec pluggableShellClassRec = {
    { /*
       *	core_class fields
       */
    /* superclass	  */	(WidgetClass) &shellClassRec,
    /* class_name	  */	"PluggableShell",
    /* size		  */	sizeof (PluggableShellRec),
    /* Class Initializer  */	NULL,
    /* class_part_initialize*/	NULL, /* XtInheritClassPartInitialize, */
    /* Class init'ed ?	  */	FALSE,
    /* initialize	  */	PluggableShellInitialize,
    /* initialize_notify  */	NULL,
    /* realize		  */	PluggableShellRealize,
    /* actions		  */	NULL,
    /* num_actions	  */	0,
    /* resources	  */	resources,
    /* resource_count	  */	XtNumber (resources),
    /* xrm_class	  */	NULLQUARK,
    /* compress_motion	  */	FALSE,
    /* compress_exposure  */	TRUE,
    /* compress_enterleave*/	FALSE,
    /* visible_interest	  */	TRUE,
    /* destroy		  */	PluggableShellDestroy, /* XtInheritDestroy, */
    /* resize		  */	XtInheritResize,
    /* expose		  */	NULL,
    /* set_values	  */	NULL, /* XtInheritSetValues, */
    /* set_values_hook	  */	NULL,
    /* set_values_almost  */	XtInheritSetValuesAlmost,
    /* get_values_hook	  */	NULL,
    /* accept_focus	  */	NULL,
    /* intrinsics version */	XtVersion,
    /* callback offsets	  */	NULL,
    /* tm_table		  */	NULL,
    /* query_geometry	  */	NULL,
    /* display_accelerator*/	NULL,
    /* extension	  */	NULL
  },{ /* Composite */
    /* geometry_manager	  */	XtInheritGeometryManager,
    /* change_managed	  */	ChangeManaged,  /* XtInheritChangeManaged */
    /* insert_child	  */	XtInheritInsertChild,
    /* delete_child	  */	XtInheritDeleteChild,
    /* extension	  */	(XtPointer)&compositeClassExtRec
  },{ /* Shell */
    /* extension	  */	(XtPointer)&shellClassExtRec
  },{ /* PluggableShell */
    0
  }
};

WidgetClass pluggableShellWidgetClass = (WidgetClass) &pluggableShellClassRec;

static void
PluggableShellInitialize (Widget req, Widget new, ArgList args,
			 Cardinal *num_args)
{
  XtAddEventHandler(new, 0,
		    TRUE, EventHandler, (XtPointer) NULL);
}

static Widget
find_managed_child (CompositeWidget w)
{
  int i;
  Widget *childP = w->composite.children;

  for (i = w->composite.num_children; i; i--, childP++)
    if (XtIsWidget(*childP) && XtIsManaged(*childP))
      return *childP;
  return NULL;
}

#ifndef XtCXtToolkitError
# define XtCXtToolkitError "XtToolkitError"
#endif

static void EventHandler(wid, closure, event, continue_to_dispatch)
     Widget wid;
     XtPointer closure;	/* unused */
     XEvent *event;
     Boolean *continue_to_dispatch; /* unused */
{
  PluggableShellWidget w = (PluggableShellWidget) wid;

  fprintf (stderr, "EventHandler - %d\n", event->type);

  if(w->core.window != event->xany.window) {
    XtAppErrorMsg(XtWidgetToApplicationContext(wid),
		  "invalidWindow","eventHandler",XtCXtToolkitError,
		  "Event with wrong window",
		  (String *)NULL, (Cardinal *)NULL);
    return;
  }
}

/* Lifted almost entirely from GetGeometry() in Shell.c
 */
static void
GetGeometry (Widget W, Widget child)
{
    PluggableShellWidget w = (PluggableShellWidget)W;
    int x, y, win_gravity = -1, flag;
    XSizeHints hints;
    Window win = w->pluggableShell.pluggable_window;

    {
      Window dummy_root;
      unsigned int dummy_bd_width, dummy_depth, width, height;

      /* determine the existing size of the window. */
      XGetGeometry(XtDisplay(W), win, &dummy_root, &x, &y, &width,
		   &height, &dummy_bd_width, &dummy_depth);
      w->core.width = width;
      w->core.height = height;

      fprintf (stderr, "GetGeometry: (%d,%d) - (%d,%d)\n",
	       x, y, width, height);
    }

    if(w->shell.geometry != NULL) {
	char def_geom[128];
	int width, height;

	x = w->core.x;
	y = w->core.y;
	width = w->core.width;
	height = w->core.height;
	hints.flags = 0;

	sprintf( def_geom, "%dx%d+%d+%d", width, height, x, y );
	flag = XWMGeometry( XtDisplay(W),
			    XScreenNumberOfScreen(XtScreen(W)),
			    w->shell.geometry, def_geom,
			    (unsigned int)w->core.border_width,
			    &hints, &x, &y, &width, &height,
			    &win_gravity
			   );
	if (flag) {
	    if (flag & XValue) w->core.x = (Position)x;
	    if (flag & YValue) w->core.y = (Position)y;
	    if (flag & WidthValue) w->core.width = (Dimension)width;
	    if (flag & HeightValue) w->core.height = (Dimension)height;
	}
	else {
	    String params[2];
	    Cardinal num_params = 2;
	    params[0] = XtName(W);
	    params[1] = w->shell.geometry;
	    XtAppWarningMsg(XtWidgetToApplicationContext(W),
       "badGeometry", "shellRealize", XtCXtToolkitError,
       "Shell widget \"%s\" has an invalid geometry specification: \"%s\"",
			    params, &num_params);
	}
    }
    else
	flag = 0;

    w->shell.client_specified |= _XtShellGeometryParsed;
}

XtGeometryResult
PluggableShellQueryGeometry (W, reply)
     Widget W;
     XtWidgetGeometry *reply;
{
    PluggableShellWidget shell = (PluggableShellWidget)W;
    Window win = shell->pluggableShell.pluggable_window;

    int x, y;
    unsigned int width, height, border_width, dummy_depth;
    Window dummy_root;
    Status retval;

    reply->request_mode = 0;

    /* determine the existing size of the window. */
    retval = XGetGeometry (XtDisplay (shell), win, &dummy_root,
			   &x, &y, &width, &height, &border_width,
			   &dummy_depth);

    if (!retval)
	return XtGeometryNo;

    reply->x = x;
    reply->y = y;
    reply->width = width;
    reply->height = height;
    reply->border_width = border_width;

    fprintf (stderr, "INIT_FRAME_SIZE: (%d,%d) - (%d,%d) - %d\n",
	     reply->x, reply->y, reply->width, reply->height,
	     reply->border_width);

    return XtGeometryYes;
}


/* Lifted almost entirely from Realize() in Shell.c
 */
static void PluggableShellRealize (Widget wid, Mask *vmask,
				   XSetWindowAttributes *attr)
{
	PluggableShellWidget w = (PluggableShellWidget) wid;
        Mask mask = *vmask;
	Window win = w->pluggableShell.pluggable_window;

	if (!win) {
	  Cardinal count = 1;
	  XtErrorMsg("invalidWindow","shellRealize", XtCXtToolkitError,
		     "No pluggable window specified for PluggableShell widget %s",
		     &wid->core.name, &count);
	}

	GetGeometry(wid, (Widget)NULL);

	if(w->shell.save_under) {
		mask |= CWSaveUnder;
		attr->save_under = TRUE;
	}
	if(w->shell.override_redirect) {
		mask |= CWOverrideRedirect;
		attr->override_redirect = TRUE;
	}
	if (wid->core.width == 0 || wid->core.height == 0) {
	    Cardinal count = 1;
	    XtErrorMsg("invalidDimension", "shellRealize", XtCXtToolkitError,
		       "Shell widget %s has zero width and/or height",
		       &wid->core.name, &count);
	}
	wid->core.window = win;
	XChangeWindowAttributes(XtDisplay(wid), wid->core.window,
				mask, attr);

}

static void PluggableShellDestroy(wid)
	Widget wid;
{
  PluggableShellWidget w = (PluggableShellWidget)wid;

  if (XtIsRealized(wid))
    PluggableShellUnrealize(wid);
}

/* Invoke matching routine from superclass, but first override its
   geometry opinions with our own routine */

static void ChangeManaged(wid)
    Widget wid;
{
  if (!XtIsRealized (wid))
    GetGeometry(wid, (Widget)NULL);
  (*((ShellClassRec*)pluggableShellClassRec.core_class.superclass)->
   composite_class.change_managed)(wid);
}

/* Based on RootGeometryManager() in Shell.c */

static XtGeometryResult PluggableShellRootGeometryManager(gw, request, reply)
    Widget gw;
    XtWidgetGeometry *request, *reply;
{
    PluggableShellWidget w = (PluggableShellWidget)gw;
    unsigned int mask = request->request_mode;
    XEvent event;
    int oldx, oldy, oldwidth, oldheight, oldborder_width;
    unsigned long request_num;
    XtWidgetGeometry req = *request; /* don't modify caller's structure */

    oldx = w->core.x;
    oldy = w->core.y;
    oldwidth = w->core.width;
    oldheight = w->core.height;
    oldborder_width = w->core.border_width;

#define PutBackGeometry() \
	{ w->core.x = oldx; \
	  w->core.y = oldy; \
	  w->core.width = oldwidth; \
	  w->core.height = oldheight; \
	  w->core.border_width = oldborder_width; }

    if (mask & CWX) {
      if (w->core.x == request->x) mask &= ~CWX;
      else
	w->core.x = request->x;
    }
    if (mask & CWY) {
      if (w->core.y == request->y) mask &= ~CWY;
      else w->core.y = request->y;
    }
    if (mask & CWBorderWidth) {
      if (w->core.border_width == request->border_width)
	      mask &= ~CWBorderWidth;
      else w->core.border_width = request->border_width;
    }
    if (mask & CWWidth) {
      if (w->core.width == request->width) mask &= ~CWWidth;
      else w->core.width = request->width;
    }
    if (mask & CWHeight) {
      if (w->core.height == request->height) mask &= ~CWHeight;
      else w->core.height = request->height;
    }

    if (!XtIsRealized((Widget)w)) return XtGeometryYes;

    fprintf (stderr, "RootGeometryManager!\n");

    req.sibling = None;
    req.request_mode = mask & ~CWSibling;
    request_num = NextRequest(XtDisplay(w));

    if (w->pluggableShell.dead_client == TRUE) {
      /* The client is sick.  Refuse the request.
       * If the client recovers and decides to honor the
       * request, it will be handled by Shell's EventHandler().
       */
      PutBackGeometry();
      return XtGeometryNo;
    }

    w->pluggableShell.dead_client = TRUE; /* timed out; must be broken */
    PutBackGeometry();
    return XtGeometryNo;
#undef PutBackGeometry
}

static void
hack_event_masks_1 (Display *display, Window w, int this_window_propagate)
{
  Window root, parent, *children;
  unsigned int nchildren;
  int i;

  if (!XQueryTree (display, w, &root, &parent, &children, &nchildren))
    return;
  for (i=0; i<nchildren; i++)
    hack_event_masks_1 (display, children[i], 1);
  if (children)
    XFree (children);
  {
    XWindowAttributes xwa;
    XSetWindowAttributes xswa;
    if (XGetWindowAttributes (display, w, &xwa)) {
      xswa.event_mask = xwa.your_event_mask & ~KeyPressMask;
      if (this_window_propagate)
	xswa.do_not_propagate_mask = xwa.do_not_propagate_mask & ~KeyPressMask;
      XChangeWindowAttributes (display, w, CWEventMask, &xswa);
    }
  }
}

/* fix all event masks on all subwindows of the specified window so that
   all key presses in any subwindow filter up to the specified window.

   We have to do this cruftiness with pluggable widgets so that we don't
   step on Motif's concept of keyboard focus.  (Due to the nature of
   Xt and Motif, X's idea of who gets the keyboard events may not jive
   with Xt's idea of same, and Xt redirects the events to the proper
   window.  This occurs on the client side and we have no knowledge
   of it, so we have to rely on a SendEvent from the client side to
   receive our keyboard events.)
*/

static void
hack_event_masks (Display *display, Window w)
{
  hack_event_masks_1 (display, w, 0);
}

/* pluggable entry points */

Bool
PluggableShellReady (Widget w, Window win, long event_mask)
{
  PluggableShellWidget ew = (PluggableShellWidget) w;
  XEvent event;
  unsigned long request_num;

  request_num = NextRequest(XtDisplay(w));
  return FALSE;
}

void
PluggableShellSetFocus (Widget wid)
{
  PluggableShellWidget w = (PluggableShellWidget) wid;
}

extern void _XtUnregisterWindow (Window, Widget);

void
PluggableShellUnrealize (Widget w)
{
#if (XT_REVISION > 5)
  XtUnregisterDrawable (XtDisplay (w), w->core.window);
#else
  extern void _XtUnregisterWindow (Window, Widget);
  _XtUnregisterWindow (w->core.window, w);
#endif
  w->core.window = 0;
}
