#include <gtk/gtksignal.h>
#include <gtk/gtkmarshal.h>
#include <bonobo.h>
#include "desktop-editor.h"
#include "desktop-editor-view.h"
#include "desktop-editor-stream.h"


static GtkObjectClass *desktop_editor_parent_class;

static CORBA_Object create_desktop_editor (BonoboObject *object);
static void editor_destroy (GtkObject *object);
static void editor_class_init (DesktopEditorClass *class);
static void editor_init (DesktopEditor *editor);

static void impl_set_client (PortableServer_Servant servant, Desktop_EditorClient client, CORBA_Environment *ev);
static Desktop_EditorStream impl_access_as_stream (PortableServer_Servant servant, CORBA_Environment *ev);

static BonoboView *embeddable_view_factory (BonoboEmbeddable *embeddable, const Bonobo_ViewFrame view_frame, void *data);

static POA_Desktop_Editor__vepv desktop_editor_vepv;

GtkType
desktop_editor_get_type (void)
{
   static GtkType type = 0;

   if (!type) {
      GtkTypeInfo info = {
	 "IDL:DESKTOP/Part:1.0",
	 sizeof (DesktopEditor),
	 sizeof (DesktopEditorClass),
	 (GtkClassInitFunc) editor_class_init,
	 (GtkObjectInitFunc) editor_init,
	 NULL, NULL,
      };
      type = gtk_type_unique (bonobo_object_get_type (), &info);
   }
   return type;
}

DesktopEditor *
desktop_editor_new (gboolean use_bonobo_embeddable)
{
   Desktop_Editor corba_editor;
   DesktopEditor *editor;
   BonoboEmbeddable *embeddable;

   editor = gtk_type_new (desktop_editor_get_type ());
   corba_editor = (Desktop_Editor)
	 create_desktop_editor (BONOBO_OBJECT (editor));
   if (corba_editor == CORBA_OBJECT_NIL) {
      gtk_object_destroy (GTK_OBJECT (editor));
      return NULL;
   }
   bonobo_object_construct (BONOBO_OBJECT (editor), corba_editor);

   if (use_bonobo_embeddable) {
      editor->be_embeddable = TRUE;
   }
   embeddable = bonobo_embeddable_new (embeddable_view_factory, NULL);
   if (!embeddable) {
      gtk_object_destroy (GTK_OBJECT (editor));
      return NULL;
   }
   bonobo_object_add_interface (BONOBO_OBJECT (editor),
				BONOBO_OBJECT(embeddable));
   return editor;
}

void
desktop_editor_set_stream (DesktopEditor *editor,
			   DesktopEditorStream *stream)
{
   g_return_if_fail (editor != NULL);
   g_return_if_fail (DESKTOP_IS_EDITOR (editor));
   g_return_if_fail (stream != NULL);
   g_return_if_fail (DESKTOP_IS_EDITOR_STREAM (stream));

   editor->stream = stream;
}

void
desktop_editor_set_view_factory (DesktopEditor *editor,
				 DesktopEditorViewFactory factory,
				 gpointer factory_closure)
{
   g_return_if_fail (editor != NULL);
   g_return_if_fail (DESKTOP_IS_EDITOR (editor));
   g_return_if_fail (factory != NULL);

   editor->factory = factory;
   editor->view_factory_closure = factory_closure;
}

POA_Desktop_Editor__epv *
desktop_editor_get_epv (void)
{
   POA_Desktop_Editor__epv *epv;

   epv = g_new0 (POA_Desktop_Editor__epv, 1);

   epv->set_client = impl_set_client;
   epv->access_as_stream = impl_access_as_stream;

   return epv;
}

static void
init_editor_corba_class (void)
{
   desktop_editor_vepv.Bonobo_Unknown_epv = bonobo_object_get_epv ();
   desktop_editor_vepv.Desktop_Editor_epv = desktop_editor_get_epv ();
}


static void
impl_set_client (PortableServer_Servant  servant,
		 Desktop_EditorClient    client,
		 CORBA_Environment      *ev)
{
}

static Desktop_EditorStream
impl_access_as_stream (PortableServer_Servant  servant,
		       CORBA_Environment      *ev)
{
   Desktop_EditorStream ret;
   CORBA_Environment evx;
   DesktopEditor *editor = DESKTOP_EDITOR (bonobo_object_from_servant (servant));

   CORBA_exception_init (&evx);
   ret = CORBA_Object_duplicate (bonobo_object_corba_objref (
	    BONOBO_OBJECT (editor->stream)), &evx);
   CORBA_exception_free (&evx);
   return ret;
}


static CORBA_Object
create_desktop_editor (BonoboObject *object)
{
   POA_Desktop_Editor *servant;
   CORBA_Environment ev;

   servant = (POA_Desktop_Editor *)g_new0 (DesktopEditor, 1);
   servant->vepv = &desktop_editor_vepv;

   CORBA_exception_init (&ev);
   POA_Desktop_Editor__init ((PortableServer_Servant) servant, &ev);

   if (ev._major != CORBA_NO_EXCEPTION) {
      g_free (servant);
      CORBA_exception_free (&ev);
      return CORBA_OBJECT_NIL;
   }
   CORBA_exception_free (&ev);
   return bonobo_object_activate_servant (object, servant);
}

static void
editor_destroy (GtkObject *object)
{
   DesktopEditor *editor = DESKTOP_EDITOR (object);

   GTK_OBJECT_CLASS (desktop_editor_parent_class)->destroy (object);
}

static void
editor_class_init (DesktopEditorClass *klass)
{
   GtkObjectClass *object_class = (GtkObjectClass *) klass;

   desktop_editor_parent_class =
      gtk_type_class (bonobo_object_get_type ());

   object_class->destroy = editor_destroy;

   init_editor_corba_class ();
}

static void
editor_init (DesktopEditor *editor)
{
   editor->views = NULL;
}

static BonoboView *
embeddable_view_factory (BonoboEmbeddable    *embeddable,
		      const Bonobo_ViewFrame  view_frame,
		      gpointer                data)
{
   BonoboObject *editor;
   DesktopEditorView *editorview;
   BonoboView *view;
   DesktopEditorViewFactory factory;

   editor = bonobo_object_query_local_interface (BONOBO_OBJECT (embeddable),
						 "IDL:Desktop/Editor:1.0");
   /* TODO: be out-of-place if be_embeddable is FALSE */
   factory = DESKTOP_EDITOR (editor)->factory;
   editorview = DESKTOP_EDITOR_VIEW_FACTORY(factory) (DESKTOP_EDITOR (editor),
						      data);

   g_list_prepend (DESKTOP_EDITOR (editor)->views, editorview);
   view = (BonoboView *)
      bonobo_object_query_local_interface (BONOBO_OBJECT (editorview),
					   "IDL:Bonobo/View:1.0");

   return view;
}
