#include <bonobo.h>
#include "desktop-editor.h"
#include "desktop-editor-factory.h"

typedef struct {
	BonoboPersistStreamIOFn load_stream;
	BonoboPersistStreamIOFn save_stream;
	BonoboPersistFileIOFn load_file;
	BonoboPersistFileIOFn save_file;
	void *closure;
	gboolean be_embeddable;
} FactoryData;

static BonoboObject *editor_factory (BonoboGenericFactory *this,
				     FactoryData *data);

BonoboGenericFactory *
desktop_editor_factory_init (BonoboPersistStreamIOFn load_stream,
			     BonoboPersistStreamIOFn save_stream,
			     BonoboPersistFileIOFn load_file,
			     BonoboPersistFileIOFn save_file,
			     void *closure,
			     gboolean be_embeddable,
			     gchar *iid)
{
	FactoryData *data;

	data = g_new0 (FactoryData, 1);
	data->load_stream = load_stream;
	data->save_stream = save_stream;
	data->load_file = load_file;
	data->save_file = save_file,
	data->closure = closure;
	data->be_embeddable = be_embeddable;

	return (bonobo_generic_factory_new (iid, editor_factory, data));
}
	
static BonoboObject *
editor_factory (BonoboGenericFactory *this, FactoryData *data)
{
	DesktopEditor *editor;
	BonoboPersistStream *pstream;
	BonoboPersistFile *pfile;

	editor = desktop_editor_new (data->be_embeddable);
	if (!editor) {
		g_error ("Couldn't create DesktopEditor");
	}

	pstream = bonobo_persist_stream_new (data->load_stream,
					     data->save_stream,
					     data->closure);
	if (!pstream) {
		g_error ("Couldn't create PersistStream");
	}
	bonobo_object_add_interface (BONOBO_OBJECT (editor),
				     BONOBO_OBJECT (pstream));

	pfile = bonobo_persist_file_new (data->load_file,
					 data->save_file,
					 data->closure);
	if (!pfile) {
		g_error ("Couldn't create PersistFile");
	}
	bonobo_object_add_interface (BONOBO_OBJECT (editor),
				     BONOBO_OBJECT (pfile));

	return (BONOBO_OBJECT (editor));
}
