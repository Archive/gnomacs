#ifndef __DESKTOP_EDITOR_FACTORY_H__
#define __DESKTOP_EDITOR_FACTORY_H__

#include <bonobo/bonobo-persist-stream.h>
#include <bonobo/bonobo-persist-file.h>

BEGIN_GNOME_DECLS

BonoboGenericFactory *desktop_editor_factory_init (BonoboPersistStreamIOFn load_stream,
						   BonoboPersistStreamIOFn save_stream,
						   BonoboPersistFileIOFn load_file,
						   BonoboPersistFileIOFn save_file,
						   void *closure,
						   gboolean be_embeddable,
						   gchar *iid);


END_GNOME_DECLS

#endif /* __DESKTOP_EDITOR_FACTORY_H__ */

