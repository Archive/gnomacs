#include <bonobo.h>
#include "DesktopEditor.h"
#include "desktop-editor-stream.h"
#include "desktop-editor-view.h"
#include "desktop-editor.h"


static GtkObjectClass *desktop_editor_view_parent_class;

static CORBA_Object create_desktop_editor_view (BonoboObject *object);
static void view_destroy (GtkObject *object);
static void view_class_init (DesktopEditorViewClass *class);
static void view_init (DesktopEditorView *view);

static void impl_get_pos (PortableServer_Servant servant,
			  Desktop_EditorView_Position *position,
			  CORBA_Environment *ev);
static void impl_scroll_pos (PortableServer_Servant servant,
			     Desktop_EditorView_Position *offset,
			     const Bonobo_Stream_SeekType whence,
			     CORBA_Environment *ev);
static Desktop_EditorView_Range impl_get_selection (PortableServer_Servant servant,
						    CORBA_Environment *ev);
static void impl_set_selection (PortableServer_Servant servant,
				const Desktop_EditorView_Range *selection,
				CORBA_Environment *ev);

struct _DesktopEditorViewPrivate {
   DesktopEditorViewGetPosFn  get_pos;
   DesktopEditorViewSetPosFn  set_pos;
   DesktopEditorViewGetSelFn  get_sel;
   DesktopEditorViewSetSelFn  set_sel;
};

static POA_Desktop_EditorView__vepv desktop_editor_view_vepv;

GtkType
desktop_editor_view_get_type (void)
{
   static GtkType type = 0;

   if (!type) {
      GtkTypeInfo info = {
	 "IDL:DESKTOP/Part:1.0",
	 sizeof (DesktopEditorView),
	 sizeof (DesktopEditorViewClass),
	 (GtkClassInitFunc) view_class_init,
	 (GtkObjectInitFunc) view_init,
	 NULL, NULL,
      };
      type = gtk_type_unique (bonobo_object_get_type (), &info);
   }
   return type;
}

DesktopEditorView *
desktop_editor_view_new (GtkWidget *widget,
			 DesktopEditorViewGetPosFn get_pos,
			 DesktopEditorViewSetPosFn set_pos,
			 DesktopEditorViewGetSelFn get_sel,
			 DesktopEditorViewSetSelFn set_sel)
{
   Desktop_EditorView corba_view;
   DesktopEditorView *view;
   BonoboView *bview;

   view = gtk_type_new (desktop_editor_view_get_type ());
   corba_view = (Desktop_EditorView)
	 create_desktop_editor_view (BONOBO_OBJECT (view));
   if (corba_view == CORBA_OBJECT_NIL) {
      gtk_object_destroy (GTK_OBJECT (view));
      return NULL;
   }

   view->priv->get_pos = get_pos;
   view->priv->set_pos = set_pos;
   view->priv->get_sel = get_sel;
   view->priv->set_sel = set_sel;

   bonobo_object_construct (BONOBO_OBJECT (view), corba_view);

   bview = bonobo_view_new (widget);
   bonobo_object_add_interface (BONOBO_OBJECT (view), BONOBO_OBJECT (bview));

   return view;
}

POA_Desktop_EditorView__epv *
desktop_editor_view_get_epv (void)
{
   POA_Desktop_EditorView__epv *epv;

   epv = g_new0 (POA_Desktop_EditorView__epv, 1);

   epv->get_pos = impl_get_pos;
   epv->scroll_pos = impl_scroll_pos;
   epv->get_selection = impl_get_selection;
   epv->set_selection = impl_set_selection;

   return epv;
}

static void
init_view_corba_class (void)
{
   desktop_editor_view_vepv.Bonobo_Unknown_epv = bonobo_object_get_epv ();
   desktop_editor_view_vepv.Desktop_EditorView_epv = desktop_editor_view_get_epv ();
}


static void
impl_get_pos (PortableServer_Servant  servant,
              Desktop_EditorView_Position *position,
	      CORBA_Environment      *ev)
{
   DesktopEditorView *view =
	 DESKTOP_EDITOR_VIEW (bonobo_object_from_servant (servant));
}

static void
impl_scroll_pos (PortableServer_Servant        servant,
	         Desktop_EditorView_Position  *offset,
	         const Bonobo_Stream_SeekType  whence,
	         CORBA_Environment            *ev)
{
   DesktopEditorView *view =
	 DESKTOP_EDITOR_VIEW (bonobo_object_from_servant (servant));
}

static Desktop_EditorView_Range
impl_get_selection (PortableServer_Servant       servant,
		    CORBA_Environment           *ev)
{
   Desktop_EditorView_Range ret;
   DesktopEditorView *view =
	 DESKTOP_EDITOR_VIEW (bonobo_object_from_servant (servant));

   ret = *(view->priv->get_sel) (view);
   return ret;
}

static void
impl_set_selection (PortableServer_Servant          servant,
		    const Desktop_EditorView_Range *selection,
		    CORBA_Environment              *ev)
{
   DesktopEditorView *view =
	 DESKTOP_EDITOR_VIEW (bonobo_object_from_servant (servant));
}


static CORBA_Object
create_desktop_editor_view (BonoboObject *object)
{
   POA_Desktop_Editor *servant;
   CORBA_Environment ev;

   servant = (POA_Desktop_Editor *)g_new0 (DesktopEditorView, 1);
   servant->vepv = &desktop_editor_view_vepv;

   CORBA_exception_init (&ev);
   POA_Desktop_Editor__init ((PortableServer_Servant) servant, &ev);

   if (ev._major != CORBA_NO_EXCEPTION) {
      g_free (servant);
      CORBA_exception_free (&ev);
      return CORBA_OBJECT_NIL;
   }
   CORBA_exception_free (&ev);
   return bonobo_object_activate_servant (object, servant);
}

static void
view_destroy (GtkObject *object)
{
   DesktopEditorView *view = DESKTOP_EDITOR_VIEW (object);

   GTK_OBJECT_CLASS (desktop_editor_view_parent_class)->destroy (object);
}

static void
view_class_init (DesktopEditorViewClass *klass)
{
   GtkObjectClass *object_class = (GtkObjectClass *) klass;

   desktop_editor_view_parent_class =
      gtk_type_class (bonobo_object_get_type ());

   object_class->destroy = view_destroy;

   init_view_corba_class ();
}

static void
view_init (DesktopEditorView *view)
{
   view->priv = g_new0 (DesktopEditorViewPrivate, 1);
}
