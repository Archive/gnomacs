#ifndef _DESKTOP_EDITOR_STREAM_H_
#define _DESKTOP_EDITOR_STREAM_H_

#include <bonobo/bonobo-stream.h>
#include "DesktopEditor.h"

BEGIN_GNOME_DECLS

#define DESKTOP_EDITOR_STREAM_TYPE        (desktop_editor_stream_get_type ())
#define DESKTOP_EDITOR_STREAM(o)          (GTK_CHECK_CAST ((o), DESKTOP_EDITOR_STREAM_TYPE, DesktopEditorStream))
#define DESKTOP_EDITOR_STREAM_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), DESKTOP_EDITOR_STREAM_TYPE, DesktopEditorStreamClass))
#define DESKTOP_IS_EDITOR_STREAM(o)       (GTK_CHECK_TYPE ((o), DESKTOP_EDITOR_STREAM_TYPE))
#define DESKTOP_IS_EDITOR_STREAM_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), DESKTOP_EDITOR_STREAM_TYPE))

typedef struct {
        BonoboStream object;
} DesktopEditorStream;

typedef struct {
	BonoboStreamClass parent_class;

	/*
	 * virtual methods
	 */
        CORBA_long    (*search)   (DesktopEditorStream *stream,
				   CORBA_char *searchstring,
				   Bonobo_Stream_SeekType whence,
				   Desktop_EditorStream_SearchOption options,
				   CORBA_Environment *ev);
} DesktopEditorStreamClass;

POA_Desktop_EditorStream__epv *desktop_editor_stream_get_epv (void);
GtkType         desktop_editor_stream_get_type     (void);

END_GNOME_DECLS

#endif /* _DESKTOP_EDITOR_STREAM_H_ */

