#ifndef __DESKTOP_EDITOR_CLIENT_H__
#define __DESKTOP_EDITOR_CLIENT_H__

#include <bonobo/bonobo.h>
#include <bonobo/bonobo-object.h>
#include "desktop-editor.h"

BEGIN_GNOME_DECLS

#define DESKTOP_EDITOR_CLIENT_TYPE        (desktop_editor_client_get_type ())
#define DESKTOP_EDITOR_CLIENT(o)          (GTK_CHECK_CAST ((o), DESKTOP_EDITOR_CLIENT_TYPE, DesktopEditorClient))
#define DESKTOP_EDITOR_CLIENT_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), DESKTOP_EDITOR_CLIENT_TYPE, DesktopEditorClientClass))
#define DESKTOP_IS_EDITOR_CLIENT(o)       (GTK_CHECK_TYPE ((o), DESKTOP_EDITOR_CLIENT_TYPE))
#define DESKTOP_IS_EDITOR_CLIENT(k) (GTK_CHECK_CLASS_TYPE ((k), DESKTOP_EDITOR_CLIENT_TYPE))


typedef struct {
	GnomeObject parent;

	Desktop_Editor editor;
	Desktop_Editor_View view;
	Desktop_Editor_Stream stream;
} DesktopEditorClient;

typedef struct {
	GnomeObjectClass parent_class;

	void (*stream_changed) (DesktopEditorClient *client);
	void (*finish) (DesktopEditorClient *client, gboolean abort);
} DesktopEditorClientClass;

GtkType desktop_editor_client_get_type (void);

DesktopEditorClient *desktop_editor_client_new ();
void desktop_editor_client_connect_editor (DesktopEditorClient *client,
					   gchar               *editor_repoid);

void desktop_editor_client_load_file      (DesktopEditorClient *client,
				           gchar               *path);
void desktop_editor_client_load_stream    (DesktopEditorClient *client,
				           BonoboStream        *stream);

END_GNOME_DECLS
#endif /* __DESKTOP_EDITOR_CLIENT_H__ */
