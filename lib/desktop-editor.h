#ifndef __DESKTOP_EDITOR_H__
#define __DESKTOP_EDITOR_H__

#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-object-client.h>
#include "DesktopEditor.h"
#include "desktop-editor-stream.h"

BEGIN_GNOME_DECLS

#define DESKTOP_EDITOR_TYPE        (desktop_editor_get_type ())
#define DESKTOP_EDITOR(o)          (GTK_CHECK_CAST ((o), DESKTOP_EDITOR_TYPE, DesktopEditor))
#define DESKTOP_EDITOR_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), DESKTOP_EDITOR_TYPE, DesktopEditorClass))
#define DESKTOP_IS_EDITOR(o)       (GTK_CHECK_TYPE ((o), DESKTOP_EDITOR_TYPE))
#define DESKTOP_IS_EDITOR_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), DESKTOP_EDITOR_TYPE))

typedef struct _DesktopEditor DesktopEditor;

#include "desktop-editor-view.h"


#define DESKTOP_EDITOR_VIEW_FACTORY(fn) ((DesktopEditorViewFactory)(fn))
typedef DesktopEditorView * (*DesktopEditorViewFactory) (DesktopEditor *editor, gpointer closure);

struct _DesktopEditor {
	BonoboObject parent;

	GList *views;

	DesktopEditorStream *stream;
	gboolean be_embeddable;
	DesktopEditorViewFactory factory;
	gpointer view_factory_closure;

	Desktop_EditorClient client;
};

typedef struct {
	BonoboObjectClass parent_class;
} DesktopEditorClass;


GtkType desktop_editor_get_type (void);
DesktopEditor * desktop_editor_new   (gboolean use_bonobo_embeddable);
void desktop_editor_set_stream       (DesktopEditor *editor,
				      DesktopEditorStream *stream);
void desktop_editor_set_view_factory (DesktopEditor *editor,
				      DesktopEditorViewFactory factory,
				      gpointer factory_closure);

POA_Desktop_Editor__epv *desktop_editor_get_epv (void);

END_GNOME_DECLS
#endif /* __DESKTOP_EDITOR_H__ */

