#include "DesktopEditor.h"
#include "desktop-editor-stream.h"

static BonoboStreamClass *desktop_editor_stream_parent_class;
#define CLASS(o) DESKTOP_EDITOR_STREAM_CLASS (GTK_OBJECT(o)->klass)


POA_Desktop_EditorStream__vepv desktop_editor_stream_vepv;

static CORBA_long
impl_search (PortableServer_Servant servant,
	     const CORBA_char *regexp,
	     const Bonobo_Stream_SeekType whence,
	     const Desktop_EditorStream_SearchOption optionmask,
	     CORBA_Environment *ev)
{
   DesktopEditorStream *stream =
      DESKTOP_EDITOR_STREAM (bonobo_object_from_servant (servant));

   return CLASS (stream)->search (stream, regexp, whence, optionmask, ev);
}

POA_Desktop_EditorStream__epv *
desktop_editor_stream_get_epv (void)
{
   POA_Desktop_EditorStream__epv *epv;

   epv = g_new0 (POA_Desktop_EditorStream__epv, 1);
   epv->search	= impl_search;

   return epv;
}

static void
init_stream_corba_class (void)
{
   /* The VEPV */
   desktop_editor_stream_vepv.Bonobo_Unknown_epv =
      bonobo_object_get_epv ();
   desktop_editor_stream_vepv.Desktop_EditorStream_epv =
      desktop_editor_stream_get_epv ();
}

static void
desktop_editor_stream_class_init (BonoboStreamClass *klass)
{
   desktop_editor_stream_parent_class =
      gtk_type_class (bonobo_stream_get_type ());

   init_stream_corba_class ();
}

GtkType
desktop_editor_stream_get_type (void)
{
   static GtkType type = 0;

   if (!type){
      GtkTypeInfo info = {
	 "BonoboStream",
	 sizeof (DesktopEditorStream),
	 sizeof (DesktopEditorStream),
	 (GtkClassInitFunc) desktop_editor_stream_class_init,
	 (GtkObjectInitFunc) NULL,
	 NULL, /* reserved 1 */
	 NULL, /* reserved 2 */
	 (GtkClassInitFunc) NULL
      };

      type = gtk_type_unique (bonobo_object_get_type (), &info);
   }

   return type;
}
