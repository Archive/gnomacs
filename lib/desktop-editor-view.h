#ifndef __DESKTOP_EDITOR_VIEW_H__
#define __DESKTOP_EDITOR_VIEW_H__

#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-object-client.h>
#include "DesktopEditor.h"

BEGIN_GNOME_DECLS


#define DESKTOP_EDITOR_VIEW_TYPE        (desktop_editor_view_get_type ())
#define DESKTOP_EDITOR_VIEW(o)          (GTK_CHECK_CAST ((o), DESKTOP_EDITOR_VIEW_TYPE, DesktopEditorView))
#define DESKTOP_EDITOR_VIEW_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), DESKTOP_EDITOR_VIEW_TYPE, DesktopEditorViewClass))
#define DESKTOP_IS_EDITOR_VIEW(o)       (GTK_CHECK_TYPE ((o), DESKTOP_EDITOR_VIEW_TYPE))
#define DESKTOP_IS_EDITOR_VIEW_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), DESKTOP_EDITOR_VIEW_TYPE))

typedef struct _DesktopEditorView DesktopEditorView;
typedef Desktop_EditorView_Position * (*DesktopEditorViewGetPosFn) (DesktopEditorView *view);
typedef gboolean (*DesktopEditorViewSetPosFn) (DesktopEditorView *view, Desktop_EditorView_Position *offset, Bonobo_Stream_SeekType whence);
typedef Desktop_EditorView_Range * (*DesktopEditorViewGetSelFn) (DesktopEditorView *view);
typedef gboolean (*DesktopEditorViewSetSelFn) (DesktopEditorView *view, Desktop_EditorView_Range *range);

typedef struct _DesktopEditorViewPrivate DesktopEditorViewPrivate;
struct _DesktopEditorView {
	BonoboObject parent;

	DesktopEditorViewPrivate *priv;
	DesktopEditorStream *stream;

};

typedef struct {
	BonoboObjectClass parent_class;

	void (*stream_changed) (DesktopEditorView *view);
} DesktopEditorViewClass;

GtkType desktop_editor_view_get_type     (void);
DesktopEditorView *desktop_editor_view_new (GtkWidget                 *widget,
					    DesktopEditorViewGetPosFn  get_pos,
					    DesktopEditorViewSetPosFn  set_pos,
					    DesktopEditorViewGetSelFn  get_sel,
					    DesktopEditorViewSetSelFn  set_sel);
void desktop_editor_view_set_stream (DesktopEditorView   *view,
				     DesktopEditorStream *stream);

POA_Desktop_EditorView__epv *desktop_editor_view_get_epv (void);
END_GNOME_DECLS
#endif /* __DESKTOP_EDITOR_VIEW_H__ */
