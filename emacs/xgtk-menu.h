#ifndef _XGtkMenu_h
#define _XGtkMenu_h

/***********************************************************************
 *
 * XGtkMenu Widget
 *
 ***********************************************************************/

#include "lwlib.h"

/* Resource names used by the XGtkMenuWidget widget */
#define XtNmenu "menu"
#define XtCMenu "Menu"
#define XtNframe "frame"
#define XtCFrame "Frame"
#define XtNopen "open"
#define XtNselect "select"

typedef struct _XGtkMenuRec *XGtkMenuWidget;
typedef struct _XGtkMenuClassRec *XGtkMenuWidgetClass;

extern WidgetClass xgtkMenuWidgetClass;

#endif /* _XGtkMenu_h */
