/* Implements a lightweight menubar widget.  
   Copyright (C) 1992 Lucid, Inc.

This file is part of the Lucid Widget Library.

The Lucid Widget Library is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

The Lucid Widget Library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GNU Emacs; see the file COPYING.  If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.  */

/* Created by devin@lucid.com */

#include <stdio.h>

#include <sys/types.h>
#include <X11/Xos.h>
#include <X11/IntrinsicP.h>
#include <X11/ObjectP.h>
#include <X11/StringDefs.h>
#include <X11/cursorfont.h>
#include <X11/bitmaps/gray>
#include "xgtk-menuP.h"

#define offset(field) XtOffset(XGtkMenuWidget, field)
static XtResource 
xgtkMenuResources[] =
{ 
  {XtNopen, XtCCallback, XtRCallback, sizeof(XtPointer), 
   offset(menu.open), XtRCallback, (XtPointer)NULL},
  {XtNselect, XtCCallback, XtRCallback, sizeof(XtPointer), 
   offset(menu.select), XtRCallback, (XtPointer)NULL},
  {XtNmenu, XtCMenu, XtRPointer, sizeof(XtPointer),
   offset(menu.contents), XtRImmediate, (XtPointer)NULL},
  {XtNframe, XtCFrame, XtRPointer, sizeof(XtPointer),
   offset(menu.frame), XtRImmediate, (XtPointer)NULL}
};
#undef offset

static void XGtkMenuInitialize();
static void XGtkMenuClassInitialize();
static void XGtkMenuRealize();
static void XGtkMenuResize();
static void XGtkMenuDestroy();
static Boolean XGtkMenuSetValues();

#define SuperClass ((CoreWidgetClass)&xgtkPlugClassRec)

XGtkMenuClassRec xgtkMenuClassRec =
{
  {  /* CoreClass fields initialization */
    (WidgetClass) SuperClass,		/* superclass		  */	
    "XGtkMenu",				/* class_name		  */
    sizeof(XGtkMenuRec),		/* size			  */
    XGtkMenuClassInitialize,		/* class_initialize	  */
    NULL,				/* class_part_initialize  */
    FALSE,				/* class_inited		  */
    XGtkMenuInitialize,			/* initialize		  */
    NULL,				/* initialize_hook	  */
    XtInheritRealize,			/* realize		  */
    NULL,				/* actions		  */
    0,					/* num_actions		  */
    xgtkMenuResources,			/* resources		  */
    XtNumber(xgtkMenuResources),	/* resource_count	  */
    NULLQUARK,				/* xrm_class		  */
    TRUE,				/* compress_motion	  */
    TRUE,				/* compress_exposure	  */
    TRUE,				/* compress_enterleave    */
    FALSE,				/* visible_interest	  */
    NULL,				/* destroy		  */
    XtInheritResize,			/* resize		  */
    NULL,				/* expose		  */
    XGtkMenuSetValues,			/* set_values		  */
    NULL,				/* set_values_hook	  */
    XtInheritSetValuesAlmost,		/* set_values_almost	  */
    NULL,				/* get_values_hook	  */
    NULL,				/* accept_focus		  */
    XtVersion,				/* version		  */
    NULL,				/* callback_private	  */
    NULL,				/* tm_table		  */
    XtInheritQueryGeometry,		/* query_geometry	  */
    XtInheritDisplayAccelerator,	/* display_accelerator	  */
    NULL				/* extension		  */
  },  /* XGtkMenuClass fields initialization */
  {
    0					/* dummy */
  },
};

WidgetClass xgtkMenuWidgetClass = (WidgetClass) &xgtkMenuClassRec;

typedef struct
{
  XGtkMenuWidget mw;
  widget_value *val;
} cbdata_t;

static void
activate_callback (GtkWidget *widget, cbdata_t *callback_data)
{
  XGtkMenuWidget mw;
  widget_value *val;

  mw = callback_data->mw;
  val = callback_data->val;

  if (mw->menu.frame)
    set_frame_menubar (mw->menu.frame, 0, 1);

  /* callback */
  XtCallCallbackList ((Widget) mw, mw->menu.select, (XtPointer) val);
}

static GnomeUIInfo *
create_menus (mw, stack, prefix)
     XGtkMenuWidget mw;
     widget_value *stack;
     char *prefix;
{
  widget_value *val;
  GnomeUIInfo *item_stack;
  int count = 0;

  for (val = stack; val; val = val->next)
    count++;

  item_stack = g_new0 (GnomeUIInfo, count+1);

  count = 0;
  for (val = stack; val; val = val->next) {
    GnomeUIInfo *info = &(item_stack [count]);
    cbdata_t *cb_data = g_new0 (cbdata_t, 1);
    gchar *path;

    path = g_strdup_printf ("%s/%s", prefix, val->name);

    if (val->contents) {
      info->type = GNOME_APP_UI_SUBTREE;
      info->moreinfo = create_menus (mw, val->contents, path);
    } else if (!strncmp (val->name, "--", 2)) {
      info->type = GNOME_APP_UI_SEPARATOR;
    } else {
      info->type = GNOME_APP_UI_ITEM;
      info->moreinfo = activate_callback;
    }

    cb_data->mw = mw;
    cb_data->val = val;

    info->label = val->name;
    info->user_data = cb_data;

    count++;
  }

  {
    GnomeUIInfo *info = &(item_stack [count]);

    info->type = GNOME_APP_UI_ENDOFINFO;
  }

  return item_stack;
}

static void
XGtkMenuInitialize (request, mw, args, num_args)
     Widget request;
     XGtkMenuWidget mw;
     ArgList args;
     Cardinal *num_args;
{
  GtkWidget *item;
  GnomeUIInfo *info;

  (*xgtkMenuWidgetClass->core_class.superclass->core_class.initialize)
    (request, (Widget) mw, args, num_args);

  mw->menu.accel_group = gtk_accel_group_new ();
  mw->menu.menu_bar = gtk_menu_bar_new ();

  mw->menu.menu_ui_info = create_menus (mw, mw->menu.contents->contents, "");

  gnome_app_fill_menu (GTK_MENU_SHELL (mw->menu.menu_bar),
		       mw->menu.menu_ui_info, NULL, FALSE, 0);

  gtk_container_add (GTK_CONTAINER (mw->plug.plug_widget), mw->menu.menu_bar);

  gtk_widget_show_all (mw->plug.plug_widget);
}

static void
XGtkMenuClassInitialize ()
{
}

static void
XGtkMenuRealize (w, valueMask, attributes)
     Widget w;
     Mask *valueMask;
     XSetWindowAttributes *attributes;
{
  (*xgtkMenuWidgetClass->core_class.superclass->core_class.realize)
    (w, valueMask, attributes);
}

static void
delete_func (GtkWidget *widget, gpointer data)
{
  XGtkMenuWidget mw = (XGtkMenuWidget) data;
  GnomeUIInfo *info = gtk_object_get_data
    (GTK_OBJECT (widget), GNOMEUIINFO_KEY_UIDATA);

  fprintf (stderr, "remove: %p - '%s' - %p\n", widget, widget->name,
	   info);

  // gtk_menu_item_remove_submenu (widget);
  gtk_container_remove (GTK_CONTAINER (mw->menu.menu_bar), widget);
}

static Boolean 
XGtkMenuSetValues (current, request, new)
     Widget current;
     Widget request;
     Widget new;
{
  XGtkMenuWidget oldmw = (XGtkMenuWidget)current;
  XGtkMenuWidget newmw = (XGtkMenuWidget)new;
  Boolean redisplay = False;

  if (newmw->menu.contents
      && newmw->menu.contents->contents
      && newmw->menu.contents->contents->change >= VISIBLE_CHANGE)
    redisplay = True;
  /* Do redisplay if the contents are entirely eliminated.  */
  if (newmw->menu.contents
      && newmw->menu.contents->contents == 0
      && newmw->menu.contents->change >= VISIBLE_CHANGE)
    redisplay = True;

  return FALSE;
}
