#ifndef _XGtkPlug_h
#define _XGtkPlug_h

/***********************************************************************
 *
 * XGtkPlug Widget
 *
 ***********************************************************************/

#include "lwlib.h"

typedef struct _XGtkPlugRec *XGtkPlugWidget;
typedef struct _XGtkPlugClassRec *XGtkPlugWidgetClass;

extern WidgetClass xgtkPlugWidgetClass;

#endif /* _XGtkPlug_h */
