/* Pluggable shell widget external header file.
   This is a special shell which can be embedded in a GtkSocket.

   Copyright (C) 2000 The Free Software Foundation.

   Contributed April 2000 by Martin Baulig <martin@home-of-linux.org>.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this library; if not, write to
   the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA. */

/* Synched up with: Not in FSF. */

#ifndef INCLUDED_PluggableShellP_h_
#define INCLUDED_PluggableShellP_h_

#include <X11/IntrinsicP.h>
#include <X11/ShellP.h>
#include "PluggableShell.h"

typedef struct {
    int dummy;
} PluggableShellClassPart;

typedef struct _PluggableShellClassRec {
    CoreClassPart core_class;
    CompositeClassPart composite_class;
    ShellClassPart shell_class;
    WMShellClassPart wm_shell_class;
    VendorShellClassPart vendor_shell_class;
    TopLevelShellClassPart top_level_shell_class;
    ApplicationShellClassPart application_shell_class;
    PluggableShellClassPart pluggableShell_class;
} PluggableShellClassRec;

typedef struct {
    Window pluggable_window;
} PluggableShellPart;

typedef struct _PluggableShellRec {
    CorePart core;
    CompositePart composite;
    ShellPart shell;
    WMShellPart wm_shell;
    VendorShellPart vendor_shell;
    TopLevelShellPart top_level_shell;
    ApplicationShellPart application_shell;
    PluggableShellPart pluggableShell;
} PluggableShellRec;

extern PluggableShellClassRec pluggableShellClassRec;

#endif /* INCLUDED_PluggableShellP_h_ */
