#!/bin/bash

if [ ! -e etc/CENSORSHIP ] ; then
  echo "You must run this script in the top-level emacs directory."
  exit 1
fi

ln -sf gnomacs/configure.in
ln -sf gnomacs/root.Makefile.in Makefile.in
ln -sf gnomacs/acinclude.m4
(cd src ; ln -sf ../gnomacs/src.Makefile.in Makefile.in)
(cd src ; ln -sf ../gnomacs/PluggableShell.c)
(cd src ; ln -sf ../gnomacs/PluggableShell.h)
(cd src ; ln -sf ../gnomacs/PluggableShellP.h)
(cd src ; ln -sf ../gnomacs/config.in)
(cd src ; ln -sf ../gnomacs/epaths.in)
(cd src ; ln -sf ../gnomacs/emacs.c)
(cd src ; ln -sf ../gnomacs/frame.c)
(cd src ; ln -sf ../gnomacs/frame.h)
(cd src ; ln -sf ../gnomacs/sysdll.c)
(cd src ; ln -sf ../gnomacs/sysdll.h)
(cd src ; ln -sf ../gnomacs/process.c)
(cd src ; ln -sf ../gnomacs/keyboard.c)
(cd src ; ln -sf ../gnomacs/xmenu.c)
(cd src ; ln -sf ../gnomacs/xfns.c)
(cd lwlib ; ln -sf ../gnomacs/lwlib.Makefile.in Makefile.in)
(cd lwlib ; ln -sf ../gnomacs/lwlib.c)
(cd lwlib ; ln -sf ../gnomacs/lwlib-Gtk.h)
(cd lwlib ; ln -sf ../gnomacs/lwlib-Gtk.c)
(cd lwlib ; ln -sf ../gnomacs/xgtk-plug.h)
(cd lwlib ; ln -sf ../gnomacs/xgtk-plugP.h)
(cd lwlib ; ln -sf ../gnomacs/xgtk-plug.c)
(cd lwlib ; ln -sf ../gnomacs/xgtk-plug-gtk.h)
(cd lwlib ; ln -sf ../gnomacs/xgtk-plug-gtk.c)
(cd lwlib ; ln -sf ../gnomacs/xgtk-menu.h)
(cd lwlib ; ln -sf ../gnomacs/xgtk-menuP.h)
(cd lwlib ; ln -sf ../gnomacs/xgtk-menu.c)
