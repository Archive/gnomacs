/* Implements a lightweight menubar widget.  
   Copyright (C) 1992 Lucid, Inc.

This file is part of the Lucid Widget Library.

The Lucid Widget Library is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

The Lucid Widget Library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GNU Emacs; see the file COPYING.  If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.  */

/* Created by devin@lucid.com */

#include <stdio.h>

#include <sys/types.h>
#include <X11/Xos.h>
#include <X11/IntrinsicP.h>
#include <X11/ObjectP.h>
#include <X11/StringDefs.h>
#include <X11/cursorfont.h>
#include <X11/bitmaps/gray>
#include "xgtk-plugP.h"
#include "xgtk-plug-gtk.h"

#define offset(field) XtOffset(XGtkPlugWidget, field)
static XtResource 
xgtkPlugResources[] =
{ 
};
#undef offset

static void XGtkPlugInitialize();
static void XGtkPlugClassInitialize();
static void XGtkPlugRealize();
static void XGtkPlugResize();
static void XGtkPlugDestroy();
static XtGeometryResult XGtkQueryGeometry ();

#define SuperClass ((CoreWidgetClass)&coreClassRec)

XGtkPlugClassRec xgtkPlugClassRec =
{
  {  /* CoreClass fields initialization */
    (WidgetClass) SuperClass,		/* superclass		  */	
    "XGtkPlug",				/* class_name		  */
    sizeof(XGtkPlugRec),		/* size			  */
    XGtkPlugClassInitialize,		/* class_initialize	  */
    NULL,				/* class_part_initialize  */
    FALSE,				/* class_inited		  */
    XGtkPlugInitialize,			/* initialize		  */
    NULL,				/* initialize_hook	  */
    XGtkPlugRealize,			/* realize		  */
    NULL,				/* actions		  */
    0,					/* num_actions		  */
    xgtkPlugResources,			/* resources		  */
    XtNumber(xgtkPlugResources),	/* resource_count	  */
    NULLQUARK,				/* xrm_class		  */
    TRUE,				/* compress_motion	  */
    TRUE,				/* compress_exposure	  */
    TRUE,				/* compress_enterleave    */
    FALSE,				/* visible_interest	  */
    XGtkPlugDestroy,			/* destroy		  */
    XGtkPlugResize,			/* resize		  */
    NULL,				/* expose		  */
    NULL,				/* set_values		  */
    NULL,				/* set_values_hook	  */
    XtInheritSetValuesAlmost,		/* set_values_almost	  */
    NULL,				/* get_values_hook	  */
    NULL,				/* accept_focus		  */
    XtVersion,				/* version		  */
    NULL,				/* callback_private	  */
    NULL,				/* tm_table		  */
    XGtkQueryGeometry,			/* query_geometry	  */
    XtInheritDisplayAccelerator,	/* display_accelerator	  */
    NULL				/* extension		  */
  },  /* XGtkPlugClass fields initialization */
  {
    0					/* dummy */
  },
};

WidgetClass xgtkPlugWidgetClass = (WidgetClass) &xgtkPlugClassRec;

static void
XGtkPlugInitialize (request, pw, args, num_args)
     Widget request;
     XGtkPlugWidget pw;
     ArgList args;
     Cardinal *num_args;
{
  pw->plug.plug_widget = xgtk_plug_gtk_new ((Widget) pw);

  pw->core.width = 1;
  pw->core.height = 1;
}

static void
XGtkPlugClassInitialize ()
{
}

static void
XGtkPlugRealize (w, valueMask, attributes)
     Widget w;
     Mask *valueMask;
     XSetWindowAttributes *attributes;
{
  (*xgtkPlugWidgetClass->core_class.superclass->core_class.realize)
    (w, valueMask, attributes);
}

static void 
XGtkPlugResize (w)
     Widget w;
{
  XGtkPlugWidget pw = (XGtkPlugWidget)w;

  fprintf (stderr, "XGtkPlugResize\n");

  gtk_widget_queue_resize (pw->plug.plug_widget);
}

static void 
XGtkPlugDestroy (w)
     Widget w;
{
  XGtkPlugWidget pw = (XGtkPlugWidget)w;

  xgtk_plug_gtk_destroy (pw->plug.plug_widget);

  (*xgtkPlugWidgetClass->core_class.superclass->core_class.destroy) (w);
}

XtGeometryResult
XGtkQueryGeometry (w, reply)
     Widget w;
     XtWidgetGeometry *reply;
{
  XGtkPlugWidget pw = (XGtkPlugWidget)w;
  GtkRequisition req;

  gtk_widget_size_request (pw->plug.plug_widget, &req);

  reply->request_mode = 0;

  reply->x = 0;
  reply->y = 0;
  reply->width = req.width;
  reply->height = req.height;
  reply->border_width = 0;

  fprintf (stderr, "initial_size: (%d,%d) - (%d,%d) - %d\n",
	   reply->x, reply->y, reply->width, reply->height,
	   reply->border_width);

  return XtGeometryYes;
}
