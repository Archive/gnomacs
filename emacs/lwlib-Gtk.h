#ifndef LWLIB_XGTK_H
#define LWLIB_XGTK_H

#include "lwlib-int.h"

extern widget_creation_entry xgtk_creation_table [];
extern widget_creation_function xgtk_create_dialog;

Boolean
lw_lucid_widget_p (/* Widget widget */);

void
xgtk_update_one_widget (/* widget_instance* instance, Widget widget,
			   widget_value* val, Boolean deep_p */);

void
xgtk_update_one_value (/* widget_instance* instance, Widget widget,
			  widget_value* val */);

void
xgtk_destroy_instance (/* widget_instance* instance */);

void
xgtk_pop_instance (/* widget_instance* instance, Boolean up */);

void
xgtk_popup_menu (/* Widget widget */);

#endif /* LWLIB_XGTK_H */
