#ifndef _XGtkPlugGtk_h
#define _XGtkPlugGtk_h

#include <gdk/gdk.h>
#include <gtk/gtkwindow.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define XGTK_PLUG_GTK(obj)		GTK_CHECK_CAST (obj, xgtk_plug_gtk_get_type (), XGtkPlugGtk)
#define XGTK_PLUG_GTK_CLASS(klass)	GTK_CHECK_CLASS_CAST (klass, xgtk_plug_gtk_get_type (), GtkPlugClass)
#define XGTK_IS_PLUG_GTK(obj)		GTK_CHECK_TYPE (obj, xgtk_plug_gtk_get_type ())

typedef struct _XGtkPlugGtk		XGtkPlugGtk;
typedef struct _XGtkPlugGtkClass	XGtkPlugGtkClass;

struct _XGtkPlugGtk
{
  GtkPlug plug;

  Widget widget;
};

struct _XGtkPlugGtkClass
{
  GtkPlugClass parent_class;
};

guint      xgtk_plug_gtk_get_type (void);
void       xgtk_plug_gtk_construct (XGtkPlugGtk *plug, Widget widget);
GtkWidget* xgtk_plug_gtk_new (Widget widget);

void       xgtk_plug_gtk_destroy (GtkWidget *widget);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _XGtkPlugGtk_h */
