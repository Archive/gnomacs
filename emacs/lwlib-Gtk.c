/* The lwlib interface to "xgtkmenu" menus.
   Copyright (C) 1992 Lucid, Inc.

This file is part of the Lucid Widget Library.

The Lucid Widget Library is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 1, or (at your option)
any later version.

The Lucid Widget Library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GNU Emacs; see the file COPYING.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.  */

#include "lwlib-Gtk.h"
#include <X11/StringDefs.h>
#include <X11/IntrinsicP.h>
#include <X11/ObjectP.h>
#include <X11/CompositeP.h>
#include <X11/Shell.h>
#include "xgtk-menu.h"

/* creation functions */

static void
pick_hook (w, client_data, call_data)
     Widget w;
     XtPointer client_data;
     XtPointer call_data;
{
  widget_instance* instance = (widget_instance*)client_data;
  widget_value* contents_val = (widget_value*)call_data;
  widget_value* widget_val;
  XtPointer widget_arg;

  if (w->core.being_destroyed)
    return;

  widget_val = lw_get_widget_value_for_widget (instance, w);
  widget_arg = widget_val ? widget_val->call_data : NULL;

  if (instance->info->pre_activate_cb)
    instance->info->pre_activate_cb (w, instance->info->id, widget_arg);

  if (instance->info->selection_cb && contents_val && contents_val->enabled
      && !contents_val->contents)
    instance->info->selection_cb (w, instance->info->id,
				  contents_val->call_data);

  if (instance->info->post_activate_cb)
    instance->info->post_activate_cb (w, instance->info->id, widget_arg);
}

static Widget
xgtk_create_menubar (instance)
     widget_instance* instance;
{
  Widget widget;
  Arg al[5];
  int ac = 0;

  XtSetArg (al[ac], XtNmenu, instance->info->val); ac++;

  /* This used to use XtVaCreateWidget, but an old Xt version
     has a bug in XtVaCreateWidget that frees instance->info->name.  */
  widget
    = XtCreateWidget (instance->info->name, xgtkMenuWidgetClass,
		      instance->parent, al, ac);

  XtAddCallback (widget, XtNselect, pick_hook, (XtPointer)instance);

  return widget;
}

static Widget
xgtk_create_popup_menu (instance)
     widget_instance* instance;
{
  Widget popup_shell
    = XtCreatePopupShell (instance->info->name, overrideShellWidgetClass,
			  instance->parent, NULL, 0);
  
  Widget widget;
  Arg al[2];
  int ac = 0;

  XtSetArg (al[ac], XtNmenu, instance->info->val); ac++;

  /* This used to use XtVaManagedCreateWidget, but an old Xt version
     has a bug in XtVaManagedCreateWidget that frees instance->info->name.  */
  widget
    = XtCreateManagedWidget ("popup", xgtkMenuWidgetClass,
			     popup_shell, al, ac);

  XtAddCallback (widget, XtNselect, pick_hook, (XtPointer)instance);

  return popup_shell;
}

widget_creation_entry 
xgtk_creation_table [] =
{
  {"menubar", xgtk_create_menubar},
  {"popup", xgtk_create_popup_menu},
  {NULL, NULL}
};

Boolean
lw_gtk_widget_p (widget)
     Widget widget;
{
  WidgetClass the_class = XtClass (widget);

  if (the_class == xgtkMenuWidgetClass)
    return True;
  if (the_class == overrideShellWidgetClass)
    return (XtClass (((CompositeWidget)widget)->composite.children [0])
	    == xgtkMenuWidgetClass);
  return False;
}

void
xgtk_update_one_widget (instance, widget, val, deep_p)
     widget_instance* instance;
     Widget widget;
     widget_value* val;
     Boolean deep_p;
{
  XGtkMenuWidget mw;
  Arg al[1];
  int ac = 0;

  if (XtIsShell (widget))
    mw = (XGtkMenuWidget)((CompositeWidget)widget)->composite.children [0];
  else
    mw = (XGtkMenuWidget)widget;

#if 0
  /* This used to use XtVaSetValues, but some old Xt versions
     that have a bug in XtVaCreateWidget might have it here too.  */
  XtSetArg (al[0], XtNmenu, instance->info->val); ac++;
#endif

  XtSetValues (widget, al, ac);
}

void
xgtk_update_one_value (instance, widget, val)
     widget_instance* instance;
     Widget widget;
     widget_value* val;
{
  return;
}

void
xgtk_pop_instance (instance, up)
     widget_instance* instance;
     Boolean up;
{
}

void
xgtk_popup_menu (widget, event)
     Widget widget;
     XEvent *event;
{
  XButtonPressedEvent dummy;
  XGtkMenuWidget mw;

  if (!XtIsShell (widget))
    return;

  mw = (XGtkMenuWidget)((CompositeWidget)widget)->composite.children [0];
}

/* Destruction of instances */
void
xgtk_destroy_instance (instance)
     widget_instance* instance;
{
  if (instance->widget)
    XtDestroyWidget (instance->widget);
}

