#ifndef _XGtkMenuP_h
#define _XGtkMenuP_h

#include "xgtk-plugP.h"
#include "xgtk-menu.h"

#include <X11/CoreP.h>
#include <gtk/gtk.h>

#include <gnome.h>

/* New fields for the XGtkMenu widget instance record */
typedef struct _XGtkMenu_part 
{
  /* slots set by the resources */
  XtCallbackList	open;
  XtCallbackList	select;
  widget_value		*contents;
  XtPointer		frame;

  /* private */
  GtkAccelGroup		*accel_group;
  GnomeUIInfo		*menu_ui_info;
  GtkWidget		*menu_bar;
} XGtkMenuPart;

/* Full instance record declaration */
typedef struct _XGtkMenuRec 
{
  CorePart	core;
  XGtkPlugPart	plug;
  XGtkMenuPart	menu;
} XGtkMenuRec;

/* New fields for the XGtkMenu widget class record */
typedef struct 
{
  int	dummy;
} XGtkMenuClassPart;

/* Full class record declaration. */
typedef struct _XGtkMenuClassRec 
{
  CoreClassPart		core_class;
  XGtkPlugClassPart	plug_class;
  XGtkMenuClassPart	menu_class;
} XGtkMenuClassRec;

/* Class pointer. */
extern XGtkMenuClassRec xgtkMenuClassRec;

#endif /* _XGtkMenuP_h */
