#ifndef _XGtkPlugP_h
#define _XGtkPlugP_h

#include "xgtk-plug.h"
#include <X11/CoreP.h>
#include <gtk/gtk.h>

/* New fields for the XGtkPlug widget instance record */
typedef struct _XGtkPlug_part 
{
  GtkWidget	*plug_widget;
} XGtkPlugPart;

/* Full instance record declaration */
typedef struct _XGtkPlugRec 
{
  CorePart	core;
  XGtkPlugPart	plug;
} XGtkPlugRec;

/* New fields for the XGtkPlug widget class record */
typedef struct 
{
  int	dummy;
} XGtkPlugClassPart;

/* Full class record declaration. */
typedef struct _XGtkPlugClassRec 
{
  CoreClassPart		core_class;
  XGtkPlugClassPart	plug_class;
} XGtkPlugClassRec;

/* Class pointer. */
extern XGtkPlugClassRec xgtkPlugClassRec;

#endif /* _XGtkPlugP_h */
