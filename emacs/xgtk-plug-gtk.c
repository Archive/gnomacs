#include <stdio.h>

#include <sys/types.h>
#include <X11/Xos.h>
#include <X11/IntrinsicP.h>
#include <X11/ObjectP.h>
#include <X11/StringDefs.h>
#include <X11/cursorfont.h>
#include <X11/bitmaps/gray>

#include "xgtk-plugP.h"
#include "xgtk-plug-gtk.h"

#include <gdk/gdkx.h>
#include <gdk/gdkkeysyms.h>

static void xgtk_plug_gtk_class_init	(XGtkPlugGtkClass *klass);
static void xgtk_plug_gtk_init		(XGtkPlugGtk	*plug);

static void xgtk_plug_gtk_realize	(GtkWidget	*widget);
static void xgtk_plug_gtk_unrealize	(GtkWidget	*widget);

static void xgtk_plug_gtk_size_request	(GtkWidget	*widget,
					 GtkRequisition	*req);

static void xgtk_plug_gtk_size_allocate	(GtkWidget	*widget,
					 GtkAllocation	*allocation);

static GtkPlugClass *parent_class = NULL;

guint
xgtk_plug_gtk_get_type ()
{
  static guint xgtk_plug_gtk_type = 0;

  if (!xgtk_plug_gtk_type)
    {
      static const GtkTypeInfo xgtk_plug_gtk_info =
      {
	"XGtkPlugGtk",
	sizeof (XGtkPlugGtk),
	sizeof (XGtkPlugGtkClass),
	(GtkClassInitFunc) xgtk_plug_gtk_class_init,
	(GtkObjectInitFunc) xgtk_plug_gtk_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL
      };

      xgtk_plug_gtk_type = gtk_type_unique (gtk_plug_get_type (),
					    &xgtk_plug_gtk_info);
    }

  return xgtk_plug_gtk_type;
}

static void
xgtk_plug_gtk_class_init (XGtkPlugGtkClass *class)
{
  GtkWidgetClass *widget_class;
  GtkPlugClass   *plug_class;

  widget_class = (GtkWidgetClass *)class;
  plug_class   = (GtkPlugClass *)class;

  parent_class = gtk_type_class (gtk_plug_get_type ());

  widget_class->realize = xgtk_plug_gtk_realize;
  widget_class->unrealize = xgtk_plug_gtk_unrealize;

  widget_class->size_request = xgtk_plug_gtk_size_request;
  widget_class->size_allocate = xgtk_plug_gtk_size_allocate;
}

static void
xgtk_plug_gtk_init (XGtkPlugGtk *plug_gtk)
{
}

void
xgtk_plug_gtk_construct (XGtkPlugGtk *widget, Widget core_widget)
{
  XGtkPlugGtk *plug_gtk;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (XGTK_IS_PLUG_GTK (widget));

  plug_gtk = XGTK_PLUG_GTK (widget);

  plug_gtk->widget = core_widget;
}

GtkWidget*
xgtk_plug_gtk_new (Widget core_widget)
{
  XGtkPlugGtk *plug_gtk;

  plug_gtk = XGTK_PLUG_GTK (gtk_type_new (xgtk_plug_gtk_get_type ()));
  xgtk_plug_gtk_construct (plug_gtk, core_widget);
  return GTK_WIDGET (plug_gtk);
}

static void
xgtk_plug_gtk_unrealize (GtkWidget *widget)
{
  XGtkPlugGtk *plug_gtk;
  GtkPlug *plug;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (XGTK_IS_PLUG_GTK (widget));

  plug_gtk = XGTK_PLUG_GTK (widget);
  plug = GTK_PLUG (widget);

  if (plug->socket_window != NULL)
    {
      gdk_window_set_user_data (plug->socket_window, NULL);
      gdk_window_destroy (plug->socket_window);
      plug->socket_window = NULL;
    }

  if (GTK_WIDGET_CLASS (parent_class)->unrealize)
    (* GTK_WIDGET_CLASS (parent_class)->unrealize) (widget);
}

static void
xgtk_plug_gtk_realize (GtkWidget *widget)
{
  XGtkPlugGtk *plug_gtk;
  GtkPlug *plug;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (XGTK_IS_PLUG_GTK (widget));

  plug_gtk = XGTK_PLUG_GTK (widget);
  plug = GTK_PLUG (widget);

  fprintf (stderr, "realize: %p - %ld\n", widget,
	   plug_gtk->widget->core.parent->core.window);

  gdk_error_trap_push ();
  plug->socket_window = gdk_window_foreign_new
    (plug_gtk->widget->core.parent->core.window);
  plug->same_app = FALSE;
  gdk_flush ();
  gdk_error_trap_pop ();

  if (GTK_WIDGET_CLASS (parent_class)->realize)
    (* GTK_WIDGET_CLASS (parent_class)->realize) (widget);
}

static void
xgtk_plug_gtk_size_request (GtkWidget *widget, GtkRequisition *req)
{
  XGtkPlugGtk *plug_gtk;
  GtkPlug *plug;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (XGTK_IS_PLUG_GTK (widget));

  plug_gtk = XGTK_PLUG_GTK (widget);
  plug = GTK_PLUG (widget);

  req->width = plug_gtk->widget->core.width;
  req->height = plug_gtk->widget->core.height;

  fprintf (stderr, "size_request: (%d,%d)\n", req->width, req->height);

  if (GTK_WIDGET_CLASS (parent_class)->size_request) {
    GtkRequisition child_req;
    gboolean resized = FALSE;

    (* GTK_WIDGET_CLASS (parent_class)->size_request) (widget, &child_req);

    fprintf (stderr, "child_size_request: (%d,%d)\n",
	     child_req.width, child_req.height);

    if (child_req.width > req->width) {
      req->width = child_req.width;
      resized = TRUE;
    }

    if (child_req.height > req->height) {
      req->height = child_req.height;
      resized = TRUE;
    }

    if (resized)
	XtResizeWidget (plug_gtk->widget, req->width, req->height,
			plug_gtk->widget->core.border_width);
  }

  fprintf (stderr, "top_size_request: (%d,%d)\n", req->width, req->height);
}

static void
xgtk_plug_gtk_size_allocate (GtkWidget *widget,
			     GtkAllocation *allocation)
{
  XGtkPlugGtk *plug_gtk;
  GtkPlug *plug;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (XGTK_IS_PLUG_GTK (widget));

  plug_gtk = XGTK_PLUG_GTK (widget);
  plug = GTK_PLUG (widget);

  fprintf (stderr, "size_allocate: (%d,%d) - (%d,%d)\n",
	   allocation->x, allocation->y,
	   allocation->width, allocation->height);

  if (GTK_WIDGET_CLASS (parent_class)->size_allocate)
    (* GTK_WIDGET_CLASS (parent_class)->size_allocate) (widget, allocation);
}

void
xgtk_plug_gtk_destroy (GtkWidget *widget)
{
  XGtkPlugGtk *plug_gtk;
  GtkPlug *plug;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (XGTK_IS_PLUG_GTK (widget));

  if (GTK_WIDGET_REALIZED (widget))
    xgtk_plug_gtk_unrealize (widget);

  gtk_widget_destroy (widget);
  gtk_main_iteration_do (FALSE);
}
