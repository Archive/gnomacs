/* Pluggable shell widget external header file.
   This is a special shell which can be embedded in a GtkSocket.

   Copyright (C) 2000 The Free Software Foundation.

   Contributed April 2000 by Martin Baulig <martin@home-of-linux.org>.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this library; if not, write to
   the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA. */

/* Synched up with: Not in FSF. */

#ifdef emacs
#include <config.h>
#endif /* emacs */

#include <stdio.h>
#include <string.h>
#include <X11/StringDefs.h>
#include <X11/IntrinsicP.h>
#include <X11/Shell.h>
#include <X11/ShellP.h>
#include <X11/Vendor.h>
#include <X11/VendorP.h>
#include "PluggableShellP.h"

static void PluggableShellRealize (Widget wid, Mask *vmask, XSetWindowAttributes
				   *attr);

static XtResource resources[] = {
#define offset(field) XtOffset(PluggableShellWidget, pluggableShell.field)
    { XtNwindow, XtCWindow,
      XtRWindow, sizeof (Window),
      offset (pluggable_window), XtRImmediate, (XtPointer)0 },
};

PluggableShellClassRec pluggableShellClassRec = {
    { /*
       *	core_class fields
       */
	/* superclass		*/	(WidgetClass) &applicationShellClassRec,
	/* class_name		*/	"PluggableShell",
	/* size			*/	sizeof (PluggableShellRec),
	/* Class Initializer	*/	NULL,
	/* class_part_initialize*/	NULL, /* XtInheritClassPartInitialize, */
	/* Class init'ed ?	*/	FALSE,
	/* initialize		*/	NULL,
	/* initialize_notify	*/	NULL,
	/* realize		*/	PluggableShellRealize,
	/* actions		*/	NULL,
	/* num_actions		*/	0,
	/* resources		*/	resources,
	/* resource_count	*/	XtNumber (resources),
	/* xrm_class		*/	NULLQUARK,
	/* compress_motion	*/	FALSE,
	/* compress_exposure	*/	TRUE,
	/* compress_enterleave	*/	FALSE,
	/* visible_interest	*/	TRUE,
	/* destroy		*/	NULL,
	/* resize		*/	XtInheritResize,
	/* expose		*/	NULL,
	/* set_values		*/	NULL,
	/* set_values_hook	*/	NULL,
	/* set_values_almost	*/	XtInheritSetValuesAlmost,
	/* get_values_hook	*/	NULL,
	/* accept_focus		*/	NULL,
	/* intrinsics version	*/	XtVersion,
	/* callback offsets	*/	NULL,
	/* tm_table		*/	NULL,
	/* query_geometry	*/	NULL,
	/* display_accelerator	*/	NULL,
	/* extension		*/	NULL
    },{ /* Composite */
	/* geometry_manager	*/	XtInheritGeometryManager,
	/* change_managed	*/	NULL,
	/* insert_child		*/	XtInheritInsertChild,
	/* delete_child		*/	XtInheritDeleteChild,
	/* extension		*/	NULL
    },{ /* Shell */
	/* extension		*/	NULL
    },{ /* WMShell */
	/* extension		*/	NULL
    },{ /* VendorShell */
	/* extension		*/	NULL
    },{ /* TopLevelShell */
	/* extension		*/	NULL
    },{ /* ApplicationShell */
	/* extension		*/	NULL
    },{ /* PluggableShell */
	0
    }
};

WidgetClass pluggableShellWidgetClass = (WidgetClass) &pluggableShellClassRec;

#ifndef XtCXtToolkitError
# define XtCXtToolkitError "XtToolkitError"
#endif

static void
SetGeometryFromSocket (Widget W)
{
    PluggableShellWidget w = (PluggableShellWidget) W;
    Window dummy_root, win = w->pluggableShell.pluggable_window;
    unsigned int dummy_bd_width, dummy_depth, width, height;
    int x, y;

    /* determine the existing size of the window. */
    XGetGeometry (XtDisplay (W), win, &dummy_root, &x, &y, &width,
		  &height, &dummy_bd_width, &dummy_depth);
    w->core.width = width;
    w->core.height = height;

    fprintf (stderr, "SetGeometryFromSocket: (%d,%d) - (%d,%d)\n",
	     x, y, width, height);

    w->shell.client_specified |= _XtShellGeometryParsed;
}

XtGeometryResult
PluggableShellQueryGeometry (Widget W, XtWidgetGeometry *reply)
{
    PluggableShellWidget shell = (PluggableShellWidget)W;
    Window dummy_root, win = shell->pluggableShell.pluggable_window;
    Status retval;

    unsigned int width, height, border_width, dummy_depth;
    int x, y;

    reply->request_mode = 0;

    /* determine the existing size of the window. */
    retval = XGetGeometry (XtDisplay (shell), win, &dummy_root,
			   &x, &y, &width, &height, &border_width,
			   &dummy_depth);

    if (!retval)
	return XtGeometryNo;

    reply->x = x;
    reply->y = y;
    reply->width = width;
    reply->height = height;
    reply->border_width = border_width;

    fprintf (stderr, "INIT_FRAME_SIZE: (%d,%d) - (%d,%d) - %d\n",
	     reply->x, reply->y, reply->width, reply->height,
	     reply->border_width);

    return XtGeometryYes;
}


static void
PluggableShellRealize (Widget wid, Mask *vmask,
		       XSetWindowAttributes *attr)
{
    PluggableShellWidget w = (PluggableShellWidget) wid;
    Window win = w->pluggableShell.pluggable_window;

    if (!win) {
	Cardinal count = 1;
	XtErrorMsg ("invalidWindow","shellRealize", XtCXtToolkitError,
		    "No pluggable window specified for PluggableShell widget %s",
		    &wid->core.name, &count);
    }

    SetGeometryFromSocket (wid);

    if (wid->core.width == 0 || wid->core.height == 0) {
	Cardinal count = 1;
	XtErrorMsg ("invalidDimension", "shellRealize", XtCXtToolkitError,
		    "Shell widget %s has zero width and/or height",
		    &wid->core.name, &count);
    }

    wid->core.window = win;
}

