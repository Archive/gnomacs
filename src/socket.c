#include <config.h>
#include <gnome.h>

#include <gdk/gdkx.h>

static void
test_cb (GtkWidget *socket, gpointer data)
{
    fprintf (stderr, "ID: %ld\n", GDK_WINDOW_XWINDOW (socket->window));
}

int
main (int argc, char **argv)
{
    GtkWidget *app, *socket;

    bindtextdomain (PACKAGE, GNOMELOCALEDIR);
    textdomain (PACKAGE);

    gnome_init ("socket", "0.01", argc, argv);

    app = gnome_app_new ("gnomacs", "Gnomacs");
    gtk_window_set_default_size (GTK_WINDOW (app), 500, 440);
    gtk_window_set_policy (GTK_WINDOW (app), TRUE, TRUE, FALSE);

    socket = gtk_socket_new ();

    gtk_signal_connect (GTK_OBJECT (socket), "realize",
			(GtkSignalFunc) test_cb, NULL);

    gnome_app_set_contents (GNOME_APP (app), socket);

    gtk_widget_show_all (app);

    gtk_main ();

    return 0;
}
