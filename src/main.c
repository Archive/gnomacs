#include <config.h>
#include <gnome.h>
#include <bonobo.h>

#include <liboaf/liboaf.h>

#ifdef GNOMACS_FSF
#define COMPONENT_OAFIID "OAFIID:embeddable:gnomacs-fsf-text-plain:49b707cd-211b-473e-a4e5-66f404030b24"
#else
#define COMPONENT_OAFIID "OAFIID:embeddable:gnomacs-text-plain:6a37681f-ce06-4d3a-8aaa-9ce859c26d7c"
#endif


static void
container_system_exception_cb (BonoboObject *container_object, 
			       CORBA_Object cobject,
			       CORBA_Environment *ev, gpointer data)
{
    gnome_warning_dialog (_("fatal CORBA exception!  Shutting down..."));

    bonobo_object_destroy (BONOBO_OBJECT (data));

    gtk_main_quit ();
}

static guint
container_create (gpointer filename)
{
    GtkWidget *app, *box, *view_widget;
    BonoboContainer *container;
    BonoboClientSite *client_site;
    BonoboObjectClient *server;
    BonoboViewFrame *view_frame;
    BonoboUIHandler *uih;

    Bonobo_PersistFile persist;
    CORBA_Environment ev;

    app = gnome_app_new ("gnomacs", "Gnomacs");
    gtk_window_set_default_size (GTK_WINDOW (app), 500, 440);
    gtk_window_set_policy (GTK_WINDOW (app), TRUE, TRUE, FALSE);

    container = bonobo_container_new ();

    /*
     * The "system_exception" signal will notify us if a fatal CORBA
     * exception has rendered the BonoboContainer defunct.
     */
    gtk_signal_connect (GTK_OBJECT (container), "system_exception",
			container_system_exception_cb, container);


    uih = bonobo_ui_handler_new ();

    bonobo_ui_handler_set_app (uih, GNOME_APP (app));
    bonobo_ui_handler_create_menubar (uih);

    box = gtk_vbox_new (FALSE, 0);
    gnome_app_set_contents (GNOME_APP (app), box);

    /*
     * The ClientSite is the container-side point of contact for
     * the Embeddable.  So there is a one-to-one correspondence
     * between BonoboClientSites and BonoboEmbeddables.
     */
    client_site = bonobo_client_site_new (container);

    /*
     * Launch the component.
     */
    server = bonobo_object_activate (COMPONENT_OAFIID, 0);

    if (server == NULL)
	g_error ("Could not launch object server.");

    /*
     * Bind it to the local ClientSite.  Every embedded component
     * has a local BonoboClientSite object which serves as a
     * container-side point of contact for the embeddable.  The
     * container talks to the embeddable through its ClientSite
     */
    if (!bonobo_client_site_bind_embeddable (client_site, server))
	g_error ("Could not bind object server.");

    /*
     * The BonoboContainer object maintains a list of the
     * ClientSites which it manages.  Here we add the new
     * ClientSite to that list.
     */
    bonobo_container_add (container, BONOBO_OBJECT (client_site));

    /*
     * Create the remote view and the local ViewFrame.  This also
     * sets the BonoboUIHandler for this ViewFrame.  That way, the
     * embedded component can get access to our UIHandler server
     * so that it can merge menu and toolbar items when it gets
     * activated.
     */
    view_frame = bonobo_client_site_new_view
	(client_site, bonobo_object_corba_objref (BONOBO_OBJECT (uih)));

    /*
     * Embed the view frame into the application.
     */
    view_widget = bonobo_view_frame_get_wrapper (view_frame);

    gtk_box_pack_start (GTK_BOX (box), view_widget, TRUE, TRUE, 0);

    CORBA_exception_init (&ev);

    persist = bonobo_object_client_query_interface
	(server, "IDL:Bonobo/PersistFile:1.0", &ev);

    if (ev._major != CORBA_NO_EXCEPTION || persist == CORBA_OBJECT_NIL)
	g_error ("component doesn't support PersistFile!");

    Bonobo_PersistFile_load (persist, (CORBA_char *) filename, &ev);

    if (ev._major != CORBA_NO_EXCEPTION) {
	gnome_warning_dialog (_("An exception occured while trying "
				"to load data into the component with "
				"PersistFile"));
    }
    if (ev._major != CORBA_SYSTEM_EXCEPTION)
	CORBA_Object_release (persist, &ev);

    Bonobo_Unknown_unref (persist, &ev);	
    CORBA_exception_free (&ev);

    gtk_widget_show_all (app);

    bonobo_view_frame_view_activate (view_frame);
    bonobo_view_frame_set_covered (view_frame, FALSE);
    
    return FALSE;
}

int
main (int argc, char **argv)
{
    CORBA_ORB orb;

    bindtextdomain (PACKAGE, GNOMELOCALEDIR);
    textdomain (PACKAGE);

    gnome_init_with_popt_table ("gnomacs", "0.01", argc, argv,
				oaf_popt_options, 0, NULL);

    orb = oaf_init (argc, argv);

    if (bonobo_init (orb, NULL, NULL) == FALSE)
	g_error (_("Could not initialize Bonobo!\n"));

    /*
     * We can't make any CORBA calls unless we're in the main
     * loop.  So we delay creating the container here.
     */
    gtk_idle_add ((GtkFunction) container_create,
		  gnome_util_home_file ("session"));

    bonobo_main ();

    return 0;
}
