#ifndef __DESKTOP_EDITOR_IDL__
#define __DESKTOP_EDITOR_IDL__

#if defined(__ORBIT_IDL__)
%{
#pragma include_defs bonobo/bonobo-object.h
#pragma include_defs bonobo/bonobo-storage.h
%}
#pragma inhibit push
#endif

#include <bonobo-unknown.idl>
#include <bonobo-storage.idl>

#if defined(__ORBIT_IDL__)
#pragma inhibit pop
#endif


module Desktop {

	interface EditorStream;
	interface EditorView;
	interface Editor;

	interface EditorClient : Bonobo::Unknown {
		/**
		 * changed:
		 *
		 * This method gets called when the contents of the data
		 * in the editor change.
		 */
		void changed ();

		/**
	         * finish:
		 * @abort: whether or not the user aborted the edit instead
		 * of wanting to save it.
		 *
		 * this callback gets called when the user has signalled
		 * he is done editing the document
		 */
		void finish (in boolean abort);
	};

	/* This interface is intended as an add-on to Bonobo::Stream
	 */
	interface EditorStream : Bonobo::Unknown {
		typedef long SearchOption;
		/* whether to ignore case while searching */
		const SearchOption SEARCH_ICASE = 1;
		/* whether to search backwards */
		const SearchOption SEARCH_REVERSE = 2;
		/* whether the search-string is a regexp */
		const SearchOption SEARCH_REGEX = 4;

		exception OptionNotSupported { SearchOption optionmask; };
		/**
		 * search:
		 * @regexp: the string or regexp to find
		 * @whence: search from where
		 * @flags: Options modifying the search
		 *
		 * lets you search for a string in the stream.
		 * Returns the position of the beginning of the searchstring
		 * or -1 if the string was not found. The search-string can
		 * optionally be a regular expression (Editors do not have
		 * to support this)
		 *
		 * NOTE: every editor needs to support this function. It
		 * can just choose not to support some of the options.
		 */
		 long search (in string                   regexp,
		              in Bonobo::Stream::SeekType whence,
		              in SearchOption             optionmask);
	};


	interface EditorView : Bonobo::Unknown {

		exception OutOfRange {};

		struct Position {
			long character;
			long row;
			long column;
		};

		struct Range {
			Position start;
			Position end;
		};

		/**
		 * get_pos:
		 * @position: an EditorViewer::Position
		 *
		 * Use this function to:
		 * a) Convert a character based position to line and column
		 *    or the vice versa.
		 * b) Return the current position of the cursor in the
		      EditorView.
		 * To get b) set both the character and row fields of
		 * @position to -1.
		 */
		void get_pos (inout Position position);

		/**
		 * scroll_pos:
		 * @offset: how much to scroll
		 * @whence: scroll from where
		 *
		 * This method allows you change the position of the cursor
		 * in the editor window. If @offset's character field is non-zero, 
		 * that will be used to scroll on a character basis. Otherwise
		 * the line field will be used to scroll on a line basis.
		 * the column field is ignored.
		 *
		 * Returns the cursor position after the scroll.
		 */
		void scroll_pos (inout Position              offset,
		                 in    Bonobo::Stream::SeekType whence) raises (OutOfRange);
		/**
		 * get_selection:
		 *
		 * Returns the range of the current selection.
		 */
		Range get_selection ();

		/**
		 * set_selection:
		 * @selection: A Range indicating what to select
		 *
		 * Sets the current selection in the view to @selection
		 */
		void set_selection (in Range selection) raises (OutOfRange);
	};


	/* the editor needs to support Bonobo::PersistFile and
	 * Bonobo::PersistStream for loading/saving files
	 */
	interface Editor : Bonobo::Unknown {
		/**
		 * set_client:
		 * @client: The Desktop::EditorClient object that gets
		 * notified of changes
		 */
		void set_client (in Desktop::EditorClient client);

		/**
		 * access_as_stream:
		 *
		 * Returns a Desktop::EditorStream object through which
		 * you can manipulate the contents of the editor.
		 */
		Desktop::EditorStream access_as_stream ();
	};
};

#endif /* __DESKTOP_EDITOR_IDL__ */
